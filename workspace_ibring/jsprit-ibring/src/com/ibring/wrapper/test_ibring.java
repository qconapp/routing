package com.ibring.wrapper;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.graphhopper.GraphHopper;
import com.graphhopper.util.shapes.GHPoint;

import com.ibring.distance.matrix.CHMatrixGeneration;
import com.ibring.distance.matrix.MatrixResult;
import com.ibring.Constraints.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import jsprit.analysis.toolbox.AlgorithmEventsRecorder;
import jsprit.analysis.toolbox.AlgorithmEventsViewer;
import jsprit.analysis.toolbox.GraphStreamViewer;
import jsprit.analysis.toolbox.Plotter;
import jsprit.analysis.toolbox.GraphStreamViewer.Label;
import jsprit.core.algorithm.VehicleRoutingAlgorithm;
import jsprit.core.algorithm.box.Jsprit;
import jsprit.core.algorithm.box.SchrimpfFactory;
import jsprit.core.algorithm.io.VehicleRoutingAlgorithms;
import jsprit.core.algorithm.selector.SelectBest;
import jsprit.core.algorithm.state.StateId;
import jsprit.core.algorithm.state.StateManager;
import jsprit.core.algorithm.termination.IterationWithoutImprovementTermination;
import jsprit.core.analysis.SolutionAnalyser;
import jsprit.core.problem.Location;
import jsprit.core.problem.VehicleRoutingProblem;
import jsprit.core.problem.VehicleRoutingProblem.FleetSize;
import jsprit.core.problem.constraint.ConstraintManager;
import jsprit.core.problem.io.VrpXMLReader;
import jsprit.core.problem.io.VrpXMLWriter;
import jsprit.core.problem.job.Delivery;
import jsprit.core.problem.job.Service;
import jsprit.core.problem.job.Shipment;
import jsprit.core.problem.solution.VehicleRoutingProblemSolution;
import jsprit.core.problem.solution.route.VehicleRoute;
import jsprit.core.problem.solution.route.activity.DeliverService;
import jsprit.core.problem.solution.route.activity.DeliverShipment;
import jsprit.core.problem.solution.route.activity.PickupShipment;
import jsprit.core.problem.solution.route.activity.TimeWindow;
import jsprit.core.problem.solution.route.activity.TourActivity;
import jsprit.core.problem.solution.route.activity.TourActivity.JobActivity;
import jsprit.core.problem.vehicle.VehicleImpl;
import jsprit.core.problem.vehicle.VehicleType;
import jsprit.core.problem.vehicle.VehicleTypeImpl;

import jsprit.core.problem.vehicle.VehicleImpl.Builder;

import jsprit.core.reporting.SolutionPrinter;
import jsprit.core.reporting.SolutionPrinter.Print;
import jsprit.core.util.Coordinate;
import jsprit.core.util.DistanceUnit;
import jsprit.core.util.EuclideanDistanceCalculator;
import jsprit.core.util.GreatCircleDistanceCalculator;
import jsprit.core.problem.cost.TransportDistance;
import jsprit.core.problem.cost.WaitingTimeCosts;
import jsprit.core.util.Solutions;
import jsprit.core.util.VehicleRoutingTransportCostsMatrix;
import jsprit.instance.reader.SolomonReader;
import jsprit.util.Examples;

import com.ibring.json.*;

import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.sql.Time;

public class test_ibring {
	
	static double distanceTimeFactor = 0.001;
	public static void main_test(String[] args) throws FileNotFoundException, IOException {
	    
		VehicleRoutingProblem.Builder vrpBuilder = VehicleRoutingProblem.Builder.newInstance();
	
		new VrpXMLReader(vrpBuilder).read("input/shipment-problem-test.xml");
	
	
		VehicleRoutingTransportCostsMatrix costMatrix = null;
       	costMatrix =createMatrix(vrpBuilder);
        
        vrpBuilder.setRoutingCost(costMatrix);
        
	    final VehicleRoutingProblem vrp = vrpBuilder.build();
	    
	    VehicleRoutingAlgorithm vra = Jsprit.createAlgorithm(vrp); 
	    vra.setMaxIterations(16);
	    
	    Collection<VehicleRoutingProblemSolution> solutions = vra.searchSolutions();
	    VehicleRoutingProblemSolution solution = new SelectBest().selectSolution(solutions);
	

        SolutionPrinter.print(vrp, solution, Print.VERBOSE);
	    
	    return;
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
    	
		if (args.length != 2)
		{
			System.out.println("Ein- und/oder Ausgabedatei nicht �bergeben!");
			System.out.println("Exit ...");
			return;
		}
		String status = "";
		String inputFile = args[0];
		String outputFile = args[1];
		
		System.out.println("Inputfile: " + inputFile);
		System.out.println("Outputfile: " + outputFile);
		
    	RoutingInformation routeingInformation = new RoutingInformation();
    	
    	Gson gson = new GsonBuilder()
       		 .registerTypeAdapter(Date.class, new DateSerializer())
       		 .registerTypeAdapter(Time.class, new TimeSerializer()).create();
    	
    	 try(Reader reader = new FileReader( inputFile )){
             routeingInformation = gson.fromJson(reader, RoutingInformation.class);
         }catch (Exception e) {
        	 status += "ERROR - read json: "  +  e.getLocalizedMessage();
        	 System.out.println(status);
        	 e.printStackTrace();
         }
    	
    	 Parameter parameter = routeingInformation.getParameter();
         VehicleRoutingProblem.Builder vrpBuilder = VehicleRoutingProblem.Builder.newInstance();
         
         try{
	         for( Vehicle vehicle:  routeingInformation.getVehicles())
	         {
	             VehicleTypeImpl.Builder vehicleTypeBuilder = VehicleTypeImpl.Builder.newInstance(vehicle.getType())
	            		.addCapacityDimension(0, vehicle.getMaxWeight())
	          			.setCostPerDistance(vehicle.getCostPerKm() / 1000)
	          			//.setCostPerWaitingTime(vehicle.getCostPerHour() / 3600)
	          			.setCostPerTransportTime(vehicle.getCostPerHour() / 3600)
	          			.setCostPerServiceTime(vehicle.getCostPerHour() / 3600)
	          			.setFixedCost(vehicle.getFixedCost());
	             VehicleType vehicleType = vehicleTypeBuilder.build();
	            
	             Builder vehicleBuilder = VehicleImpl.Builder.newInstance(String.valueOf(vehicle.getId()));
	             vehicleBuilder.setStartLocation(loc(Coordinate.newInstance(vehicle.getLon(), vehicle.getLat())));
	             //vehicleBuilder.setReturnToDepot(true);
	             vehicleBuilder.setType(vehicleType);
	             vehicleBuilder.setEarliestStart(vehicle.getStartTimeSec());
	             vehicleBuilder.setLatestArrival(vehicle.getEndTimeSec());
	             vrpBuilder.addVehicle(vehicleBuilder.build());
	         }
	         
	         if (parameter.isFleetSizeFinite()){
	        	 vrpBuilder.setFleetSize(FleetSize.FINITE);
	         }else{
	        	 vrpBuilder.setFleetSize(FleetSize.INFINITE);
	         }
    	 }catch (Exception e) {
    		 status += "ERROR - build fleet: "  +  e.getLocalizedMessage();
        	 System.out.println(status);
        	 e.printStackTrace();
         }
	     
         Boolean depotBasedDelivery = false;
         if (parameter.isDepotBasedDelivery()){
        	 depotBasedDelivery = true;
        	 Job lastJob = null;
        	 for( Job job:  routeingInformation.getJobs())
	         {
        		 if (lastJob != null && (
        				 lastJob.getPickup().getLon() != job.getPickup().getLon() ||
        			     lastJob.getPickup().getLat() !=  job.getPickup().getLat() )){
        			 
        			 depotBasedDelivery = false;
        			 break;
        		 }
        		 lastJob = job;
	         }
         }
         
         List<Long> depotBasedJobPickups = new ArrayList<Long>();
         double loadingTimeDepotBasedJobs = 0.0;
         if (depotBasedDelivery){
        	 Delivery service;
             try{
    	         for( Job job:  routeingInformation.getJobs())
    	         {
    	        	 service = Delivery.Builder.newInstance(String.valueOf(job.getId()))
    	        			 					// .setName(String.valueOf())
    	        			 					.addSizeDimension(0, job.getWeight())
    	        			 					.setLocation(loc(Coordinate.newInstance(job.getDelivery().getLon(), job.getDelivery().getLat())))
    	        			 					.setServiceTime(job.getUnloadingSec())
    	        			 					.setServiceTimeRepeated(job.getUnloadingSec() * parameter.getMultipleUnloadingFactor())
    	        			 					.setTimeWindow(TimeWindow.newInstance(job.getDelivery().getStartTimeSec(),job.getDelivery().getEndTimeSec()))
    	        			 					.build();
    	        	 depotBasedJobPickups.add(job.getId());
    	        	 
    	        	 if (loadingTimeDepotBasedJobs > 0.0)
    	        		 loadingTimeDepotBasedJobs += job.getLoadingSec() * parameter.getMultipleLoadingFactor();
    	        	 else
    	        		 loadingTimeDepotBasedJobs += job.getLoadingSec();
    	        	 
    	        	 vrpBuilder.addJob(service);
    	         }
             }catch (Exception e) {
        		 status += "ERROR - build services: "  +  e.getLocalizedMessage();
            	 System.out.println(status);
            	 e.printStackTrace();
             }
         }else{
	         Shipment shipment;
	         try{
		         for( Job job:  routeingInformation.getJobs())
		         {
		        	 shipment = Shipment.Builder.newInstance(String.valueOf(job.getId()))
		        			 					//.setName(String.valueOf(job.getId()) + "_###")
		        			 					.addSizeDimension(0, job.getWeight())
		        			 					.setPickupLocation(loc(Coordinate.newInstance(job.getPickup().getLon(), job.getPickup().getLat())))
		        			 					.setPickupServiceTime(job.getLoadingSec())
		        			 					.setPickupServiceTimeRepeated (job.getLoadingSec() * parameter.getMultipleLoadingFactor())
		        			 					.setPickupTimeWindow(TimeWindow.newInstance(job.getPickup().getStartTimeSec(),job.getPickup().getEndTimeSec()))
		        			 					.setDeliveryLocation(loc(Coordinate.newInstance(job.getDelivery().getLon(), job.getDelivery().getLat())))
		        			 					.setDeliveryServiceTime(job.getUnloadingSec())
		        			 					.setDeliveryServiceTimeRepeated(job.getUnloadingSec() * parameter.getMultipleUnloadingFactor())
		        			 					.setDeliveryTimeWindow(TimeWindow.newInstance(job.getDelivery().getStartTimeSec(),job.getDelivery().getEndTimeSec()))
		        			 					.build();
		        	 
		         
		        	 vrpBuilder.addJob(shipment);
		         }
	         }catch (Exception e) {
	    		 status += "ERROR - build shipment: "  +  e.getLocalizedMessage();
	        	 System.out.println(status);
	        	 e.printStackTrace();
	         }
         }
         int pickupStopId = 0;
         for( Tour tour: routeingInformation.getTours())
         {
        	 jsprit.core.problem.solution.route.VehicleRoute.Builder initialRoute = VehicleRoute.Builder.newInstance(getVehicle(String.valueOf(tour.getVehicleId()), vrpBuilder));
        	 
        	 for( Stop stop: tour.getStops())
	         {
        		 for( SignedJob signedJob: stop.getSignedJobs()){
        			 
        			 if (depotBasedDelivery){
        				 if (signedJob.getType().startsWith("pickup")){
        					 pickupStopId = stop.getStopId();
        				 }
        				 if (signedJob.getType().startsWith("deliver")){
	        				 Service service = getService(String.valueOf(signedJob.getJobId()), vrpBuilder);
	        				 service.setServiceStopId(stop.getStopId());
	        				 initialRoute.addService(service);
        				 }
        			 }
        			 else{
        				 Shipment shipment = getShipment(String.valueOf(signedJob.getJobId()), vrpBuilder);
        				 
        				 if (signedJob.getType().startsWith("pickup")){
        					 shipment.setPickupStopId(stop.getStopId());
        					 initialRoute.addPickup(shipment);
            			 }else if (signedJob.getType().startsWith("deliver")){
            				 shipment.setDeliveryStopId(stop.getStopId());
            				 initialRoute.addDelivery(shipment);
            			 }
        			 }
        				 
	        	 }
	         }
        	 vrpBuilder.addInitialVehicleRoute(initialRoute.build()); 
         }
         
         routeingInformation.setTours(new ArrayList<Tour>());
         routeingInformation.setUnassignedJobs(new ArrayList<Integer>());
         
         VehicleRoutingTransportCostsMatrix costMatrixTmp = null;
         try{
	         if (!parameter.getGraphhopperMaps().isEmpty()){
	        	 costMatrixTmp = createMatrixHopper(vrpBuilder, parameter.getGraphhopperMaps());
	         }else{
	        	 costMatrixTmp = createMatrix(vrpBuilder);
	         }
	         vrpBuilder.setRoutingCost(costMatrixTmp);
         }catch (Exception e) {
    		 status += "ERROR - build cost matrix: "  +  e.getLocalizedMessage();
        	 System.out.println(status);
        	 e.printStackTrace();
         }
         final VehicleRoutingTransportCostsMatrix costMatrix = costMatrixTmp;
         
         VehicleRoutingProblem problem = null;
         VehicleRoutingProblemSolution bestSolution = null;
         
         try{
	        problem = vrpBuilder.build();
	        
	        problem.getJobs().containsKey("1");
	        //VehicleRoutingAlgorithm algorithm = Jsprit.createAlgorithm(problem); 
	        //VehicleRoutingAlgorithm algorithm = new SchrimpfFactory().createAlgorithm(problem);
	        
	        jsprit.core.algorithm.box.Jsprit.Builder builder = Jsprit.Builder.newInstance(problem);
	 	   
	        StateManager stateManager = new StateManager(problem);
	        stateManager.addStateUpdater(new ServiceTimeUpdater());
	        
	        StateId distanceStateId = null;
	        StateId transportTimeStateId = null;
	        if (parameter.getKmLimit() > 0){
		        distanceStateId = stateManager.createStateId("distance");
		 	    stateManager.addStateUpdater(new DistanceUpdater(distanceStateId, stateManager, costMatrix));
	        }
	        if (parameter.getHourLimit() > 0){
	           transportTimeStateId = stateManager.createStateId("transportTime");
	 	       stateManager.addStateUpdater(new TransportTimeUpdater(transportTimeStateId, stateManager, costMatrix));
		    }
	       
	       ConstraintManager constraintManager = new ConstraintManager(problem, stateManager);
	       
	       if (parameter.getKmLimit() > 0){
	    	   constraintManager.addConstraint(new DistanceConstraint(parameter.getKmLimit() * 1000., distanceStateId, stateManager, costMatrix), ConstraintManager.Priority.CRITICAL);
	       }
	       
	       if (parameter.getHourLimit() > 0){
	        	constraintManager.addConstraint(new TransportTimeConstraint(parameter.getHourLimit() * 60. *60. , transportTimeStateId, stateManager, costMatrix), ConstraintManager.Priority.CRITICAL);
	 	   }
	        
	       builder.addCoreStateAndConstraintStuff(true);
	       builder.setStateAndConstraintManager(stateManager, constraintManager);
	       
	       VehicleRoutingAlgorithm algorithm = builder.buildAlgorithm();
	        
	        if (parameter.getIterations() > 0){
	        	 algorithm.setMaxIterations(parameter.getIterations());
	        }else{
	        	 algorithm.setMaxIterations(100);
	        }
	       
	
	        //algorithm.setPrematureAlgorithmTermination(new IterationWithoutImprovementTermination(10));
	
	        Collection<VehicleRoutingProblemSolution> solutions = algorithm.searchSolutions();
			
	        bestSolution = Solutions.bestOf(solutions);
	
	        Collection<VehicleRoutingProblemSolution> bestSolutions = new ArrayList<VehicleRoutingProblemSolution> ();
	        
	        bestSolutions.add(bestSolution);
	        
	        new VrpXMLWriter(problem, bestSolutions).write("output/shipment-problem-with-solution_old.xml");
	      
			SolutionPrinter.print(problem, bestSolution, Print.VERBOSE);
         }catch (Exception e) {
    		 status += "ERROR - search solution: "  +  e.getLocalizedMessage();
        	 System.out.println(status);
        	 e.printStackTrace();
         }

        SolutionAnalyser analyser = new SolutionAnalyser(problem,bestSolution, new TransportDistance() {
            @Override
            public double getDistance(Location from, Location to) {
            	return costMatrix.getTransportCost(from, to, 0., null, null);
            }
        });
        
        Tour tour;
        Job job;
        int routeNumber = 0;
        
        try{
	        for (VehicleRoute route : bestSolution.getRoutes()) {
	        	routeNumber -= 1;	        	
	        	tour = new Tour( routeNumber, Integer.valueOf(route.getVehicle().getId()));
	        	
	        	double startTime = 0.0; 
	        	boolean firstJob = true;
	        	for (TourActivity act : route.getActivities()) {
	            	
	            	String jobId;
	            	if (act instanceof JobActivity) {
	                    jobId = ((JobActivity) act).getJob().getId();
	                } else {
	                    jobId = "-";
	                }
	            	
	            	Double arrTime= 0.0;
	            	double loadingStopTime = loadingTimeDepotBasedJobs;
	            	double loadingFirstWaiting = analyser.getWaitingTimeAtActivity(act, route) - loadingStopTime;
	            	if (firstJob){
	            		arrTime = act.getEndTime() - act.getOperationTime();
	            		startTime = arrTime.doubleValue();
	            		if (depotBasedDelivery){
	            		    
	            		    for (TourActivity pickupAct : route.getActivities()) {
	        	            	
	        	            	String pickupJobId;
	        	            	if (pickupAct instanceof JobActivity) {
	        	            		pickupJobId = ((JobActivity) pickupAct).getJob().getId();
	        	            /*
	            			for (Long pickupJobId_ : depotBasedJobPickups) {
	            			*/
	        	        		tour.addJob(pickupStopId,
	        	        				Integer.parseInt(pickupJobId), 
	        	        				"pickup",
	        		            		0,
	        		            		route.getVehicle().getEarliestDeparture(), 
	        		            		arrTime -  analyser.getTransportTimeAtActivity(act, route), 
	        		            		route.getVehicle().getStartLocation().getCoordinate().getX(),
	        		            		route.getVehicle().getStartLocation().getCoordinate().getY(),
	        		            		loadingFirstWaiting,
	        		            		loadingStopTime);
	        	        		loadingStopTime = 0.0;
	        	        		loadingFirstWaiting = 0.0;
	        	            	}
	        	        	}
		        		}
	            	}else{
	            		arrTime = act.getArrTime();
	            	}
	            	
	            	System.out.println(jobId + ' ' + act.getName());
	            	System.out.println("WaitingTime: " + analyser.getWaitingTimeAtActivity(act, route)); 
	 	            System.out.println("Distance: " + analyser.getDistanceAtActivity(act, route));
	 	            System.out.println("TransportTime: " + analyser.getTransportTimeAtActivity(act, route));

	 	            System.out.println("ArrTime: " +  arrTime);
	 	            System.out.println("EndTime: " +  act.getEndTime());
	 	            System.out.println("OperationTime: " +  act.getOperationTime() );

	 	            System.out.println("***************************** " );
	 	           
	 	           switch(act.getClass().getSimpleName()){
		 	          case "DeliverService":
		 	        	 tour.addJob(Integer.valueOf(((Service)((DeliverService)act).getJob()).getServiceStopId()),
		 	            		Integer.valueOf(jobId), 
		 	            		"delivery",
		 	            		act.getIndex(),
		 	            		arrTime, 
		 	            		act.getEndTime(), 
		 	            		act.getLocation().getCoordinate().getX(),
		 	            		act.getLocation().getCoordinate().getY(),
		 	            		firstJob ? 0.0 : analyser.getWaitingTimeAtActivity(act, route) ,
		 	            		act.getOperationTime());
		 	              break;
		 	          case "PickupShipment":
		 	        	 tour.addJob(((Shipment)((PickupShipment) act).getJob()).getPickupStopId(),
			 	            		Integer.valueOf(jobId), 
			 	            		"pickup",
			 	            		act.getIndex(),
			 	            		arrTime, 
			 	            		act.getEndTime(), 
			 	            		act.getLocation().getCoordinate().getX(),
			 	            		act.getLocation().getCoordinate().getY(),
			 	            		analyser.getWaitingTimeAtActivity(act, route),
			 	            		act.getOperationTime());
		 	              break;
		 	          case "DeliverShipment":
		 	        	 tour.addJob( ((Shipment)((DeliverShipment) act).getJob()).getDeliveryStopId(),
			 	            		Integer.valueOf(jobId), 
			 	            		"delivery",
			 	            		act.getIndex(),
			 	            		arrTime, 
			 	            		act.getEndTime(), 
			 	            		act.getLocation().getCoordinate().getX(),
			 	            		act.getLocation().getCoordinate().getY(),
			 	            		analyser.getWaitingTimeAtActivity(act, route),
			 	            		act.getOperationTime());
		 	              break;
		 	      }
	        		
	 	           firstJob = false;	            	
	            }
	            
	        	tour.setStartTimeSec(route.getVehicle().getEarliestDeparture());
	        	tour.setEndTimeSec(route.getEnd().getArrTime());
	        	tour.setOperationTimeSec(analyser.getOperationTime(route) );
	        	tour.setServiceTimeSec( analyser.getServiceTime(route) + loadingTimeDepotBasedJobs);
	        	tour.setTransportTimeSec(analyser.getTransportTime(route));
	        	tour.setWaitingTimeSec(analyser.getWaitingTime(route));
	        	
	        	tour.setTotalDistance( analyser.getDistance(route));
	        	tour.setFixedCosts(analyser.getFixedCosts(route));
	        	tour.setVariableTransportCosts(analyser.getVariableTransportCosts(route));
	        	tour.setTimeWindowViolationSec( analyser.getTimeWindowViolation(route));
	        	
	        	System.out.println("------");
	        	
	            System.out.println("vehicleId: " + route.getVehicle().getId());
	            //System.out.println("vehicleCapacity: " + route.getVehicle().getType().getCapacityDimensions() + " maxLoad: " + analyser.getMaxLoad(route));
	            System.out.println("totalDistance: " + analyser.getDistance(route));
	            System.out.println("jobSize: " +route.getTourActivities().jobSize());

	            System.out.println("waitingTime: " + analyser.getWaitingTime(route)); 
	            
	            System.out.println("operationTime: " + analyser.getOperationTime(route));
	            System.out.println("serviceTime: " + analyser.getServiceTime(route));
	            System.out.println("transportTime: " + analyser.getTransportTime(route));
	            System.out.println("transportCosts: " + analyser.getVariableTransportCosts(route));
	            System.out.println("fixedCosts: " + analyser.getFixedCosts(route));
	            System.out.println("--");

	            routeingInformation.addTour(tour);
	        }
	        
	        System.out.println("overAllTotalDistance: " + analyser.getDistance());
            
	        for (jsprit.core.problem.job.Job unassignedJob : bestSolution.getUnassignedJobs()) {
	        	routeingInformation.addUnassignedJobs(Integer.valueOf(unassignedJob.getId()));
	        }
        }catch (Exception e) {
	   		 status += "ERROR - parse solution: "  +  e.getLocalizedMessage();
	       	 System.out.println(status);
	       	 e.printStackTrace();
        }
        
        if (status.isEmpty()){
        	status = "OK";
        }
        
        routeingInformation.setStatus(status);
        try (Writer writer = new FileWriter(outputFile)) {
	        gson.toJson(routeingInformation, writer);
	    }catch (Exception e) {
	   		 status += "ERROR - write solution: "  +  e.getLocalizedMessage();
	       	 System.out.println(status);
	       	 e.printStackTrace();
        }
    }
     
    private static Location loc(Coordinate coordinate) {
    	return Location.Builder.newInstance().setCoordinate(coordinate).setId(new GHPoint(coordinate.getY(), coordinate.getX()).toString()).build();
    }
     
    private static VehicleRoutingTransportCostsMatrix createMatrix(VehicleRoutingProblem.Builder vrpBuilder) {
        VehicleRoutingTransportCostsMatrix.Builder matrixBuilder = VehicleRoutingTransportCostsMatrix.Builder.newInstance(true);
        for (String from : vrpBuilder.getLocationMap().keySet()) {
            for (String to : vrpBuilder.getLocationMap().keySet()) {
                Coordinate fromCoord = vrpBuilder.getLocationMap().get(from);
                Coordinate toCoord = vrpBuilder.getLocationMap().get(to);
                double distance = GreatCircleDistanceCalculator.calculateDistance(fromCoord, toCoord,  DistanceUnit.Kilometer) ;
                matrixBuilder.addTransportDistance(from, to, distance);
                matrixBuilder.addTransportTime(from, to,  distance / distanceTimeFactor);
            }
        }
        return matrixBuilder.build();
    }

    private static VehicleRoutingTransportCostsMatrix createMatrixHopper(VehicleRoutingProblem.Builder vrpBuilder, String graphFolder) {
    	Set<Entry<String, Coordinate>> list = vrpBuilder.getLocationMap().entrySet();
    	
    	System.out.println( "Calculating " +list.size() + "x" +list.size() + " matrix using Graphhopper.");
     	System.out.println("--");
		
		int n = list.size();
		int i = 0;
        GHPoint []ghPoints = new GHPoint[n];
        String []pointIds = new String[n];
        
		for(Map.Entry<String, Coordinate> entry:list){
			ghPoints[i] = new GHPoint(entry.getValue().getY(), entry.getValue().getX());
			pointIds[i] = entry.getKey();
			i++;
		}
		CHMatrixGeneration gen = new CHMatrixGeneration(graphFolder);
	
		MatrixResult result =  gen.calculateMatrix(ghPoints);
		VehicleRoutingTransportCostsMatrix.Builder matrixBuilder = VehicleRoutingTransportCostsMatrix.Builder.newInstance(true);
		for (int ifrom = 0; ifrom < n; ifrom++) {
			for (int ito = 0; ito < n; ito++) {
				if(!result.isInfinite(ifrom, ito)){
					if (pointIds[ito] == ghPoints[ito].toString()){
						ito = ito+1-1;
					} 
					matrixBuilder.addTransportDistance(pointIds[ifrom], pointIds[ito], result.getDistanceMetres(ifrom,ito) );
	                matrixBuilder.addTransportTime(pointIds[ifrom], pointIds[ito],  result.getTimeMilliseconds(ifrom, ito) * .001);
	                				
				}else{
					matrixBuilder.addTransportDistance(ghPoints[ifrom].toString(), ghPoints[ito].toString(), Double.POSITIVE_INFINITY);
	                matrixBuilder.addTransportTime(ghPoints[ifrom].toString(), ghPoints[ito].toString(),  Double.POSITIVE_INFINITY);
				}
			}
		}
		
        return matrixBuilder.build();
    }

    private static Service getService(String serviceId, jsprit.core.problem.VehicleRoutingProblem.Builder vrpBuilder) {
        for (jsprit.core.problem.job.Job j : vrpBuilder.getAddedJobs()) {
            if (j.getId().equals(serviceId)) {
                return (Service) j;
            }
        }
        return null;
    }
    
    private static Shipment getShipment(String serviceId, jsprit.core.problem.VehicleRoutingProblem.Builder vrpBuilder) {
        for (jsprit.core.problem.job.Job j : vrpBuilder.getAddedJobs()) {
            if (j.getId().equals(serviceId)) {
                return (Shipment) j;
            }
        }
        return null;
    }

    private static jsprit.core.problem.vehicle.Vehicle getVehicle(String vehicleId, jsprit.core.problem.VehicleRoutingProblem.Builder vrpBuilder) {
        for (jsprit.core.problem.vehicle.Vehicle v : vrpBuilder.getAddedVehicles()) {
            if (v.getId().equals(vehicleId)) return v;
        }
        return null;
    }


}
