package com.ibring.wrapper;

import com.google.gson.Gson;

import com.google.gson.GsonBuilder;
import com.graphhopper.util.shapes.GHPoint;

import com.ibring.distance.matrix.CHMatrixGeneration;
import com.ibring.distance.matrix.MatrixResult;
import com.ibring.Constraints.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import jsprit.core.algorithm.VehicleRoutingAlgorithm;
import jsprit.core.algorithm.box.Jsprit;
import jsprit.core.algorithm.state.StateId;
import jsprit.core.algorithm.state.StateManager;
import jsprit.core.analysis.SolutionAnalyser;
import jsprit.core.problem.Location;
import jsprit.core.problem.VehicleRoutingProblem;
import jsprit.core.problem.VehicleRoutingProblem.FleetSize;
import jsprit.core.problem.constraint.ConstraintManager;
import jsprit.core.problem.io.VrpXMLReader;
import jsprit.core.problem.io.VrpXMLWriter;
import jsprit.core.problem.job.Delivery;
import jsprit.core.problem.job.Service;
import jsprit.core.problem.job.Shipment;
import jsprit.core.problem.solution.VehicleRoutingProblemSolution;
import jsprit.core.problem.solution.route.VehicleRoute;
import jsprit.core.problem.solution.route.activity.DeliverService;
import jsprit.core.problem.solution.route.activity.DeliverShipment;
import jsprit.core.problem.solution.route.activity.PickupShipment;
import jsprit.core.problem.solution.route.activity.TimeWindow;
import jsprit.core.problem.solution.route.activity.TourActivity;
import jsprit.core.problem.solution.route.activity.TourActivity.JobActivity;
import jsprit.core.problem.vehicle.VehicleImpl;
import jsprit.core.problem.vehicle.VehicleType;
import jsprit.core.problem.vehicle.VehicleTypeImpl;

import jsprit.core.problem.vehicle.VehicleImpl.Builder;

import jsprit.core.reporting.SolutionPrinter;
import jsprit.core.reporting.SolutionPrinter.Print;
import jsprit.core.util.Coordinate;
import jsprit.core.util.DistanceUnit;
import jsprit.core.util.GreatCircleDistanceCalculator;
import jsprit.core.problem.cost.TransportDistance;
import jsprit.core.util.Solutions;
import jsprit.core.util.VehicleRoutingTransportCostsMatrix;

import com.ibring.json.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Time;

public class Vrp_ibring {
	
	static double distanceTimeFactor = 0.001;
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
    	
		if (args.length != 2)
		{
			System.out.println("Ein- und/oder Ausgabedatei nicht �bergeben!");
			System.out.println("Exit ...");
			return;
		}
		
		String inputFile = args[0];
		String outputFile = args[1];
		
		System.out.println("Inputfile: " + inputFile);
		System.out.println("Outputfile: " + outputFile);
		
    	RoutingInformation routingInformation = loadRoutingInformationFromJSON(inputFile);
    	   	
    	VehicleRoutingProblem.Builder vrpBuilder = VehicleRoutingProblem.Builder.newInstance();
    	 
         
    	addFleet(routingInformation, vrpBuilder);
    	
    	Boolean depotBasedDelivery = checkIsDepotBasedDelivery(routingInformation);
         
         //List<Long> depotBasedJobPickups = new ArrayList<Long>();
         double loadingTimeDepotBasedJobs = 0.0;
         if (depotBasedDelivery){
        	 loadingTimeDepotBasedJobs = addDeliveryJobs( routingInformation, vrpBuilder);
         }else{
	         addShipmentJobs( routingInformation, vrpBuilder);
         }
         
         int pickupStopId = 0;
         
         pickupStopId = addInitialRoute(routingInformation, vrpBuilder, depotBasedDelivery);
         
         routingInformation.setTours(new ArrayList<Tour>());
         routingInformation.setUnassignedJobs(new ArrayList<Integer>());
         
         VehicleRoutingTransportCostsMatrix costMatrix = calculateCostMatrix(routingInformation, vrpBuilder);
         
         VehicleRoutingProblem problem = createProblem(routingInformation, vrpBuilder);
         VehicleRoutingProblemSolution bestSolution = calculateSolution(routingInformation, problem, costMatrix);// calculateSolution(routingInformation, vrpBuilder, problem, costMatrix);
        
         final VehicleRoutingTransportCostsMatrix costMatrixF = costMatrix;
         SolutionAnalyser analyser = new SolutionAnalyser(problem, bestSolution, new TransportDistance() {
		    @Override
		    public double getDistance(Location from, Location to) {
		    	return costMatrixF.getTransportCost(from, to, 0., null, null);
		    	
		    	
		    }
         });
        
         addSolution(routingInformation, bestSolution, analyser, depotBasedDelivery, pickupStopId, loadingTimeDepotBasedJobs);
         
         System.exit(writeSolutionToJSON(routingInformation, outputFile));
    }
     
	static RoutingInformation loadRoutingInformationFromJSON(String inputFile){
		
		RoutingInformation routeingInformation = new RoutingInformation();
    	
    	Gson gson = new GsonBuilder()
       		 .registerTypeAdapter(Date.class, new DateSerializer())
       		 .registerTypeAdapter(Time.class, new TimeSerializer()).create();
    	
    	 try(Reader reader = new FileReader( inputFile )){
             routeingInformation = gson.fromJson(reader, RoutingInformation.class);
         }catch (Exception e) {
        	 String status = "ERROR - read json: "  +  e.getLocalizedMessage();
        	 routeingInformation.addStatus(status);
        	 System.out.println(status);
        	 e.printStackTrace();
         }
    	 
    	 return routeingInformation;
	}
	
	static OsrmResultV2 loadOsrmV2JSON(String input){
		
		OsrmResultV2 osrmResult = new OsrmResultV2();
    	
    	Gson gson = new GsonBuilder()
       		 .registerTypeAdapter(Date.class, new DateSerializer())
       		 .registerTypeAdapter(Time.class, new TimeSerializer()).create();
    	
    	 try{
    		 osrmResult = gson.fromJson(input, OsrmResultV2.class);
         }catch (Exception e) {
        	 String status = "ERROR - read json: "  +  e.getLocalizedMessage();
        	 System.out.println(status);
        	 e.printStackTrace();
         }
    	 
    	 return osrmResult;
	}

	static OsrmResult loadOsrmJSON(String input){
		
		OsrmResult osrmResult = new OsrmResult();
    	
    	Gson gson = new GsonBuilder()
       		 .registerTypeAdapter(Date.class, new DateSerializer())
       		 .registerTypeAdapter(Time.class, new TimeSerializer()).create();
    	
    	 try{
    		 osrmResult = gson.fromJson(input, OsrmResult.class);
         }catch (Exception e) {
        	 String status = "ERROR - read json: "  +  e.getLocalizedMessage();
        	 System.out.println(status);
        	 e.printStackTrace();
         }
    	 
    	 return osrmResult;
	}
	
	static OsrmResult loadOsrmJSON(Reader input){
		
		OsrmResult osrmResult = new OsrmResult();
    	
    	Gson gson = new GsonBuilder()
       		 .registerTypeAdapter(Date.class, new DateSerializer())
       		 .registerTypeAdapter(Time.class, new TimeSerializer()).create();
    	
    	 try{
    		 osrmResult = gson.fromJson(input, OsrmResult.class);
         }catch (Exception e) {
        	 String status = "ERROR - read json: "  +  e.getLocalizedMessage();
        	 System.out.println(status);
        	 e.printStackTrace();
         }
    	 
    	 return osrmResult;
	}
	static int writeSolutionToJSON(RoutingInformation routingInformation, String outputFile){
		
		int ret = 0;
		
		Gson gson = new GsonBuilder()
	       		 .registerTypeAdapter(Date.class, new DateSerializer())
	       		 .registerTypeAdapter(Time.class, new TimeSerializer()).create();
		
		if (routingInformation.getStatus().isEmpty()){
			routingInformation.setStatus("OK");
	    }else{
	    	ret = 1;
	    }
	    
	    try (Writer writer = new FileWriter(outputFile)) {
	        gson.toJson(routingInformation, writer);
	    }catch (Exception e) {
	   		 String status = "ERROR - write solution: "  +  e.getLocalizedMessage();
	       	 System.out.println(status);
	       	 e.printStackTrace();
	       	 ret = 1;
	    }
	    
	    return ret;
	}
	static boolean addFleet(RoutingInformation routingInformation, VehicleRoutingProblem.Builder vrpBuilder){
		try{
	        for( Vehicle vehicle:  routingInformation.getVehicles())
	        {
	            VehicleTypeImpl.Builder vehicleTypeBuilder = VehicleTypeImpl.Builder.newInstance(vehicle.getType())
	           		.addCapacityDimension(0, vehicle.getMaxWeight())
	         			.setCostPerDistance(vehicle.getCostPerKm() / 1000)
	         			//.setCostPerWaitingTime(vehicle.getCostPerHour() / 3600)
	         			.setCostPerTransportTime(vehicle.getCostPerHour() / 3600)
	         			.setCostPerServiceTime(vehicle.getCostPerHour() / 3600)
	         			.setFixedCost(vehicle.getFixedCost());
	            VehicleType vehicleType = vehicleTypeBuilder.build();
	           
	            Builder vehicleBuilder = VehicleImpl.Builder.newInstance(String.valueOf(vehicle.getId()));
	            if ( vehicle.getStartLocation() != null ){
	            	vehicleBuilder.setStartLocation(loc(Coordinate.newInstance(vehicle.getStartLocation().getLon(), vehicle.getStartLocation().getLat())));
	            }else{
	            	vehicleBuilder.setStartLocation(loc(Coordinate.newInstance(vehicle.getLon(), vehicle.getLat())));
	            }
	            
	            if ( vehicle.getEndLocation() != null ){
	            	vehicleBuilder.setEndLocation(loc(Coordinate.newInstance(vehicle.getEndLocation().getLon(), vehicle.getEndLocation().getLat())));
	            }
	            vehicleBuilder.setReturnToDepot(true);
	            vehicleBuilder.setType(vehicleType);
	            vehicleBuilder.setEarliestStart(vehicle.getStartTimeSec());
	            vehicleBuilder.setLatestArrival(vehicle.getEndTimeSec());
	            vrpBuilder.addVehicle(vehicleBuilder.build());
	        }
	        
	        if (routingInformation.getParameter().isFleetSizeFinite()){
	       	 vrpBuilder.setFleetSize(FleetSize.FINITE);
	        }else{
	       	 vrpBuilder.setFleetSize(FleetSize.INFINITE);
	        }
		 }catch (Exception e) {
			 String status = "ERROR - build fleet: "  +  e.getLocalizedMessage();
			 routingInformation.addStatus(status);
			 System.out.println(status);
			 e.printStackTrace();
			 return false;
	    }
		return true;
	}
	
	static boolean checkIsDepotBasedDelivery(RoutingInformation routingInformation){
		if (routingInformation.getParameter().isDepotBasedDelivery()){
			Job lastJob = null;
       	 	for( Job job:  routingInformation.getJobs())
       	 	{
       	 		if (lastJob != null && (
       				 lastJob.getPickup().getLon() != job.getPickup().getLon() ||
       			     lastJob.getPickup().getLat() !=  job.getPickup().getLat() )){
       			 
       	 			return false;
       	 		}
       	 		lastJob = job;
       	 	}
       	 	return true;
        }
        return false;
	}
	
	static double addDeliveryJobs(RoutingInformation routingInformation, VehicleRoutingProblem.Builder vrpBuilder){
		double loadingTimeDepotBasedJobs = 0.0;
		
		Delivery service;
		try{
		     for( Job job:  routingInformation.getJobs())
		     {
		    	 service = Delivery.Builder.newInstance(String.valueOf(job.getId()))
		    			 					// .setName(String.valueOf())
		    			 					.addSizeDimension(0, job.getWeight())
		    			 					.setLocation(loc(Coordinate.newInstance(job.getDelivery().getLon(), job.getDelivery().getLat())))
		    			 					.setServiceTime(job.getUnloadingSec())
		    			 					.setServiceTimeRepeated(job.getUnloadingSec() * routingInformation.getParameter().getMultipleUnloadingFactor())
		    			 					.setTimeWindow(TimeWindow.newInstance(job.getDelivery().getStartTimeSec(),job.getDelivery().getEndTimeSec()))
		    			 					.build();
		    	 
		    	 if (loadingTimeDepotBasedJobs > 0.0)
		    		 loadingTimeDepotBasedJobs += job.getLoadingSec() * routingInformation.getParameter().getMultipleLoadingFactor();
		    	 else
		    		 loadingTimeDepotBasedJobs += job.getLoadingSec();
		    	 
		    	 vrpBuilder.addJob(service);
		     }
		}catch (Exception e) {
			 String status = "ERROR - build services: "  +  e.getLocalizedMessage();
			 routingInformation.addStatus(status);
			 System.out.println(status);
			 e.printStackTrace();
		}
		
		return loadingTimeDepotBasedJobs;
	}
	
	static void addShipmentJobs(RoutingInformation routingInformation, VehicleRoutingProblem.Builder vrpBuilder){
		Shipment shipment;
        try{
	         for( Job job:  routingInformation.getJobs())
	         {
	        	 shipment = Shipment.Builder.newInstance(String.valueOf(job.getId()))
	        			 					//.setName(String.valueOf(job.getId()) + "_###")
	        			 					.addSizeDimension(0, job.getWeight())
	        			 					.setPickupLocation(loc(Coordinate.newInstance(job.getPickup().getLon(), job.getPickup().getLat())))
	        			 					.setPickupServiceTime(job.getLoadingSec())
	        			 					.setPickupServiceTimeRepeated (job.getLoadingSec() * routingInformation.getParameter().getMultipleLoadingFactor())
	        			 					.setPickupTimeWindow(TimeWindow.newInstance(job.getPickup().getStartTimeSec(),job.getPickup().getEndTimeSec()))
	        			 					.setDeliveryLocation(loc(Coordinate.newInstance(job.getDelivery().getLon(), job.getDelivery().getLat())))
	        			 					.setDeliveryServiceTime(job.getUnloadingSec())
	        			 					.setDeliveryServiceTimeRepeated(job.getUnloadingSec() * routingInformation.getParameter().getMultipleUnloadingFactor())
	        			 					.setDeliveryTimeWindow(TimeWindow.newInstance(job.getDelivery().getStartTimeSec(),job.getDelivery().getEndTimeSec()))
	        			 					.build();
	        	 
	        	 vrpBuilder.addJob(shipment);
	         }
        }catch (Exception e) {
	   		 String status = "ERROR - build shipment: "  +  e.getLocalizedMessage();
			 routingInformation.addStatus(status);
	       	 System.out.println(status);
	       	 e.printStackTrace();
        }
	}
	
	static int addInitialRoute(RoutingInformation routingInformation, VehicleRoutingProblem.Builder vrpBuilder, boolean depotBasedDelivery){
		int depotBasedPickupStopId = 0;
		
        for( Tour tour: routingInformation.getTours())
        {
       	 jsprit.core.problem.solution.route.VehicleRoute.Builder initialRoute = VehicleRoute.Builder.newInstance(getVehicle(String.valueOf(tour.getVehicleId()), vrpBuilder));
       	 
       	 for( Stop stop: tour.getStops())
	         {
       		 for( SignedJob signedJob: stop.getSignedJobs()){
       			 
       			 if (depotBasedDelivery){
       				 if (signedJob.getType().startsWith("pickup")){
       					depotBasedPickupStopId = stop.getStopId();
       				 }
       				 if (signedJob.getType().startsWith("deliver")){
	        				 Service service = getService(String.valueOf(signedJob.getJobId()), vrpBuilder);
	        				 service.setServiceStopId(stop.getStopId());
	        				 initialRoute.addService(service);
       				 }
       			 }
       			 else{
       				 Shipment shipment = getShipment(String.valueOf(signedJob.getJobId()), vrpBuilder);
       				 
       				 if (signedJob.getType().startsWith("pickup")){
       					 shipment.setPickupStopId(stop.getStopId());
       					 initialRoute.addPickup(shipment);
           			 }else if (signedJob.getType().startsWith("deliver")){
           				 shipment.setDeliveryStopId(stop.getStopId());
           				 initialRoute.addDelivery(shipment);
           			 }
       			 }
       				 
	        	 }
	         }
       	 vrpBuilder.addInitialVehicleRoute(initialRoute.build()); 
        }
        
        return depotBasedPickupStopId;
	}
	
	static VehicleRoutingTransportCostsMatrix calculateCostMatrix(RoutingInformation routingInformation, VehicleRoutingProblem.Builder vrpBuilder){
		 VehicleRoutingTransportCostsMatrix costMatrixTmp = null;
         try{
        	 if (!routingInformation.getParameter().getOsrmIpPort().isEmpty()){
        		 
        		 if (routingInformation.getParameter().getOsrmVersion().compareToIgnoreCase("v0") == 0){
        			 costMatrixTmp = createMatrixOSRM(vrpBuilder, routingInformation);
        		 }else {
        			 costMatrixTmp = createMatrixOSRMV2(vrpBuilder, routingInformation);
        		 }
	         }

        	 if (costMatrixTmp == null && !routingInformation.getParameter().getGraphhopperMaps().isEmpty()){
	        	 costMatrixTmp = createMatrixHopper(vrpBuilder, routingInformation.getParameter().getGraphhopperMaps());
	         }
        	 
        	 if (costMatrixTmp == null) {
	        	 costMatrixTmp = createMatrix(vrpBuilder);
	         }
	         
	         vrpBuilder.setRoutingCost(costMatrixTmp);
         }catch (Exception e) {
    		 String status = "ERROR - build cost matrix: "  +  e.getLocalizedMessage();
    		 routingInformation.addStatus(status);
        	 System.out.println(status);
        	 e.printStackTrace();
         }
         
         return costMatrixTmp;
	}

	static VehicleRoutingProblem createProblem(RoutingInformation routingInformation, VehicleRoutingProblem.Builder vrpBuilder){
		VehicleRoutingProblem problem = null;
		try{
			 problem = vrpBuilder.build();
		}catch (Exception e) {
			 String status = "ERROR - build problem: "  +  e.getLocalizedMessage();
			 routingInformation.addStatus(status);
			 System.out.println(status);
			 e.printStackTrace();
	    }
		return problem;
	}
	
	static VehicleRoutingProblemSolution calculateSolution(RoutingInformation routingInformation, VehicleRoutingProblem problem, VehicleRoutingTransportCostsMatrix costMatrix){
		//static VehicleRoutingProblemSolution calculateSolution(RoutingInformation routingInformation, VehicleRoutingProblem.Builder vrpBuilder, VehicleRoutingProblem problem, VehicleRoutingTransportCostsMatrix costMatrix){

        VehicleRoutingProblemSolution bestSolution = null;
        
        try{
	        //problem = vrpBuilder.build();
	        
	        //VehicleRoutingAlgorithm algorithm = Jsprit.createAlgorithm(problem); 
	        //VehicleRoutingAlgorithm algorithm = new SchrimpfFactory().createAlgorithm(problem);
	        
	        jsprit.core.algorithm.box.Jsprit.Builder builder = Jsprit.Builder.newInstance(problem);
	 	   
	        if (routingInformation.getParameter().getThreads() > 0){
	        	builder.setProperty(Jsprit.Parameter.THREADS, String.valueOf(routingInformation.getParameter().getThreads()));
	        }
	        
	        builder.setProperty(Jsprit.Parameter.FAST_REGRET, "true");
	        
	        StateManager stateManager = new StateManager(problem);
	        stateManager.addStateUpdater(new ServiceTimeUpdater());
	        
	        StateId distanceStateId = null;
	        StateId transportTimeStateId = null;
	        if (routingInformation.getParameter().getKmLimit() > 0){
		        distanceStateId = stateManager.createStateId("distance");
		 	    stateManager.addStateUpdater(new DistanceUpdater(distanceStateId, stateManager, costMatrix));
	        }
	        if (routingInformation.getParameter().getHourLimit() > 0){
	           transportTimeStateId = stateManager.createStateId("transportTime");
	 	       stateManager.addStateUpdater(new TransportTimeUpdater(transportTimeStateId, stateManager, costMatrix));
		    }
	       
	       ConstraintManager constraintManager = new ConstraintManager(problem, stateManager);
	       
	       if (routingInformation.getParameter().getKmLimit() > 0){
	    	   constraintManager.addConstraint(new DistanceConstraint(routingInformation.getParameter().getKmLimit() * 1000., distanceStateId, stateManager, costMatrix), ConstraintManager.Priority.CRITICAL);
	       }
	       
	       if (routingInformation.getParameter().getHourLimit() > 0){
	        	constraintManager.addConstraint(new TransportTimeConstraint(routingInformation.getParameter().getHourLimit() * 60. *60. , transportTimeStateId, stateManager, costMatrix), ConstraintManager.Priority.CRITICAL);
	 	   }
	        
	       builder.addCoreStateAndConstraintStuff(true);
	       builder.setStateAndConstraintManager(stateManager, constraintManager);
	       
	       VehicleRoutingAlgorithm algorithm = builder.buildAlgorithm();
	        
	        if (routingInformation.getParameter().getIterations() > 0){
	        	 algorithm.setMaxIterations(routingInformation.getParameter().getIterations());
	        }else{
	        	 algorithm.setMaxIterations(100);
	        }
	       
	
	        //algorithm.setPrematureAlgorithmTermination(new IterationWithoutImprovementTermination(10));
	
	        Collection<VehicleRoutingProblemSolution> solutions = algorithm.searchSolutions();
			
	        bestSolution = Solutions.bestOf(solutions);
	
	        /*
	        Collection<VehicleRoutingProblemSolution> bestSolutions = new ArrayList<VehicleRoutingProblemSolution> ();
	        bestSolutions.add(bestSolution);
	        new VrpXMLWriter(problem, bestSolutions).write("output/shipment-problem-with-solution_v3.xml");
	        */
	        
			SolutionPrinter.print(problem, bestSolution, Print.VERBOSE);
        }catch (Exception e) {
   		 	String status = "ERROR - search solution: "  +  e.getLocalizedMessage();
   		 	routingInformation.addStatus(status);
   		 	System.out.println(status);
   		 	e.printStackTrace();
        }

        return bestSolution;
	}
	
	static void addSolution(RoutingInformation routingInformation, VehicleRoutingProblemSolution bestSolution, SolutionAnalyser analyser
								, boolean depotBasedDelivery, int depotBasedPickupStopId, double loadingTimeDepotBasedJobs){
		Tour tour;
        int routeNumber = 0;
        
        try{
	        for (VehicleRoute route : bestSolution.getRoutes()) {
	        	routeNumber -= 1;	        	
	        	tour = new Tour( routeNumber, Integer.valueOf(route.getVehicle().getId()));
	        	
	        	boolean firstJob = true;
	        	
	        	for (TourActivity act : route.getActivities()) {
	            	
	            	String jobId;
	            	if (act instanceof JobActivity) {
	                    jobId = ((JobActivity) act).getJob().getId();
	                } else {
	                    jobId = "-";
	                }
	            	
	            	Double arrTime= 0.0;
	            	double loadingStopTime = loadingTimeDepotBasedJobs;
	            	double loadingFirstWaiting = analyser.getWaitingTimeAtActivity(act, route) - loadingStopTime;
	            	if (firstJob){
	            		arrTime = act.getEndTime() - act.getOperationTime();
	            		if (depotBasedDelivery){
	            		    
	            		    for (TourActivity pickupAct : route.getActivities()) {
	        	            	
	        	            	String pickupJobId;
	        	            	if (pickupAct instanceof JobActivity) {
	        	            		pickupJobId = ((JobActivity) pickupAct).getJob().getId();
	        	            /*
	            			for (Long pickupJobId_ : depotBasedJobPickups) {
	            			*/
	        	        		tour.addJob(depotBasedPickupStopId,
	        	        				Integer.parseInt(pickupJobId), 
	        	        				"pickup",
	        		            		0,
	        		            		route.getVehicle().getEarliestDeparture(), 
	        		            		arrTime -  analyser.getTransportTimeAtActivity(act, route), 
	        		            		route.getVehicle().getStartLocation().getCoordinate().getX(),
	        		            		route.getVehicle().getStartLocation().getCoordinate().getY(),
	        		            		loadingFirstWaiting,
	        		            		loadingStopTime);
	        	        		loadingStopTime = 0.0;
	        	        		loadingFirstWaiting = 0.0;
	        	            	}
	        	        	}
		        		}
	            	}else{
	            		arrTime = act.getArrTime();
	            	}
	            	
	            	
	            	System.out.println(jobId + ' ' + act.getName());
	            	System.out.println("WaitingTime: " + analyser.getWaitingTimeAtActivity(act, route)); 
	 	            System.out.println("Distance: " + analyser.getDistanceAtActivity(act, route));
	 	            System.out.println("TransportTime: " + analyser.getTransportTimeAtActivity(act, route));

	 	            System.out.println("ArrTime: " +  arrTime);
	 	            System.out.println("EndTime: " +  act.getEndTime());
	 	            System.out.println("OperationTime: " +  act.getOperationTime() );

	 	            System.out.println("***************************** " );
	 	           
	 	           switch(act.getClass().getSimpleName()){
		 	          case "DeliverService":
		 	        	 tour.addJob(Integer.valueOf(((Service)((DeliverService)act).getJob()).getServiceStopId()),
		 	            		Integer.valueOf(jobId), 
		 	            		"delivery",
		 	            		act.getIndex(),
		 	            		arrTime, 
		 	            		act.getEndTime(), 
		 	            		act.getLocation().getCoordinate().getX(),
		 	            		act.getLocation().getCoordinate().getY(),
		 	            		firstJob ? 0.0 : analyser.getWaitingTimeAtActivity(act, route) ,
		 	            		act.getOperationTime());
		 	              break;
		 	          case "PickupShipment":
		 	        	 tour.addJob(((Shipment)((PickupShipment) act).getJob()).getPickupStopId(),
			 	            		Integer.valueOf(jobId), 
			 	            		"pickup",
			 	            		act.getIndex(),
			 	            		arrTime, 
			 	            		act.getEndTime(), 
			 	            		act.getLocation().getCoordinate().getX(),
			 	            		act.getLocation().getCoordinate().getY(),
			 	            		analyser.getWaitingTimeAtActivity(act, route),
			 	            		act.getOperationTime());
		 	              break;
		 	          case "DeliverShipment":
		 	        	 tour.addJob( ((Shipment)((DeliverShipment) act).getJob()).getDeliveryStopId(),
			 	            		Integer.valueOf(jobId), 
			 	            		"delivery",
			 	            		act.getIndex(),
			 	            		arrTime, 
			 	            		act.getEndTime(), 
			 	            		act.getLocation().getCoordinate().getX(),
			 	            		act.getLocation().getCoordinate().getY(),
			 	            		analyser.getWaitingTimeAtActivity(act, route),
			 	            		act.getOperationTime());
		 	              break;
		 	      }
	        		
	 	           firstJob = false;	            	
	            }
	        	 
	        	tour.addJob( 0,
 	            		0, 
 	            		"end",
 	            		route.getEnd().getIndex(),
 	            		route.getEnd().getArrTime(), 
 	            		route.getEnd().getArrTime(), 
 	            		route.getEnd().getLocation().getCoordinate().getX(),
 	            		route.getEnd().getLocation().getCoordinate().getY(),
 	            		0,
 	            		0);
	            
	        	tour.setStartTimeSec(route.getVehicle().getEarliestDeparture());
	        	tour.setEndTimeSec(route.getEnd().getArrTime());
	        	tour.setOperationTimeSec(analyser.getOperationTime(route) );
	        	tour.setServiceTimeSec( analyser.getServiceTime(route) + loadingTimeDepotBasedJobs);
	        	tour.setTransportTimeSec(analyser.getTransportTime(route));
	        	tour.setWaitingTimeSec(analyser.getWaitingTime(route));
	        	
	        	tour.setTotalDistance( analyser.getDistance(route));
	        	tour.setFixedCosts(analyser.getFixedCosts(route));
	        	tour.setVariableTransportCosts(analyser.getVariableTransportCosts(route));
	        	tour.setTimeWindowViolationSec( analyser.getTimeWindowViolation(route));
	        	
	        	System.out.println("------");
	        	
	            System.out.println("vehicleId: " + route.getVehicle().getId());
	            //System.out.println("vehicleCapacity: " + route.getVehicle().getType().getCapacityDimensions() + " maxLoad: " + analyser.getMaxLoad(route));
	            System.out.println("totalDistance: " + analyser.getDistance(route));
	            System.out.println("jobSize: " +route.getTourActivities().jobSize());

	            System.out.println("waitingTime: " + analyser.getWaitingTime(route)); 
	            
	            System.out.println("operationTime: " + analyser.getOperationTime(route));
	            System.out.println("serviceTime: " + analyser.getServiceTime(route));
	            System.out.println("transportTime: " + analyser.getTransportTime(route));
	            System.out.println("transportCosts: " + analyser.getVariableTransportCosts(route));
	            System.out.println("fixedCosts: " + analyser.getFixedCosts(route));
	            System.out.println("--");

	            routingInformation.addTour(tour);
	        }
	        
	        System.out.println("overAllTotalDistance: " + analyser.getDistance());
            
	        for (jsprit.core.problem.job.Job unassignedJob : bestSolution.getUnassignedJobs()) {
	        	routingInformation.addUnassignedJobs(Integer.valueOf(unassignedJob.getId()));
	        }
        }catch (Exception e) {
	   		 String status = "ERROR - parse solution: "  +  e.getLocalizedMessage();
	   		 routingInformation.addStatus(status);
	       	 System.out.println(status);
	       	 e.printStackTrace();
        }
        
	}
	
    private static Location loc(Coordinate coordinate) {
    	return Location.Builder.newInstance().setCoordinate(coordinate).setId(new GHPoint(coordinate.getY(), coordinate.getX()).toString()).build();
    }
     
    private static VehicleRoutingTransportCostsMatrix createMatrix(VehicleRoutingProblem.Builder vrpBuilder) {
        VehicleRoutingTransportCostsMatrix.Builder matrixBuilder = VehicleRoutingTransportCostsMatrix.Builder.newInstance(true);
        for (String from : vrpBuilder.getLocationMap().keySet()) {
            for (String to : vrpBuilder.getLocationMap().keySet()) {
                Coordinate fromCoord = vrpBuilder.getLocationMap().get(from);
                Coordinate toCoord = vrpBuilder.getLocationMap().get(to);
                double distance = GreatCircleDistanceCalculator.calculateDistance(fromCoord, toCoord,  DistanceUnit.Kilometer) ;
                matrixBuilder.addTransportDistance(from, to, distance);
                matrixBuilder.addTransportTime(from, to,  distance / distanceTimeFactor);
            }
        }
        return matrixBuilder.build();
    }

    private static VehicleRoutingTransportCostsMatrix createMatrixHopper(VehicleRoutingProblem.Builder vrpBuilder, String graphFolder) {
    	Set<Entry<String, Coordinate>> list = vrpBuilder.getLocationMap().entrySet();
    	
    	System.out.println( "Calculating " +list.size() + "x" +list.size() + " matrix using Graphhopper.");
     	System.out.println("--");
		
		int n = list.size();
		int i = 0;
        GHPoint []ghPoints = new GHPoint[n];
        String []pointIds = new String[n];
        
		for(Map.Entry<String, Coordinate> entry:list){
			ghPoints[i] = new GHPoint(entry.getValue().getY(), entry.getValue().getX());
			pointIds[i] = entry.getKey();
			i++;
		}
		CHMatrixGeneration gen = new CHMatrixGeneration(graphFolder);
	
		MatrixResult result =  gen.calculateMatrix(ghPoints);
		VehicleRoutingTransportCostsMatrix.Builder matrixBuilder = VehicleRoutingTransportCostsMatrix.Builder.newInstance(true);
		for (int ifrom = 0; ifrom < n; ifrom++) {
			for (int ito = 0; ito < n; ito++) {
				if(!result.isInfinite(ifrom, ito)){
					if (pointIds[ito] == ghPoints[ito].toString()){
						ito = ito+1-1;
					} 
					matrixBuilder.addTransportDistance(pointIds[ifrom], pointIds[ito], result.getDistanceMetres(ifrom,ito) );
	                matrixBuilder.addTransportTime(pointIds[ifrom], pointIds[ito],  result.getTimeMilliseconds(ifrom, ito) * .001);

	    		    //System.out.println(ifrom + ";" + ito + ";" + result.getTimeMilliseconds(ifrom, ito) );
	                				
				}else{
					matrixBuilder.addTransportDistance(ghPoints[ifrom].toString(), ghPoints[ito].toString(), Double.POSITIVE_INFINITY);
	                matrixBuilder.addTransportTime(ghPoints[ifrom].toString(), ghPoints[ito].toString(),  Double.POSITIVE_INFINITY);
				}
			}
		}
		
        return matrixBuilder.build();
    }
    
    private static VehicleRoutingTransportCostsMatrix createMatrixOSRM(VehicleRoutingProblem.Builder vrpBuilder, RoutingInformation routingInformation) {
    	
    	Set<Entry<String, Coordinate>> list = vrpBuilder.getLocationMap().entrySet();
    	String queryString = new String();
    	OsrmResult ret = null;
    	VehicleRoutingTransportCostsMatrix.Builder matrixBuilder = null;
    	
    	int n = list.size();
 		int i = 0;
    	GHPoint []ghPoints = new GHPoint[n];
        String []pointIds = new String[n];
       
		
    	for(Map.Entry<String, Coordinate> entry:list){
    		queryString += "&loc=" + String.format(Locale.ENGLISH,"%f,%f", entry.getValue().getY(), entry.getValue().getX());
    		ghPoints[i] = new GHPoint(entry.getValue().getY(), entry.getValue().getX());
			pointIds[i] = entry.getKey();
			i++;
		}
    	
		try {
			URL url = new URL("http://" + routingInformation.getParameter().getOsrmIpPort() + "/table?" + queryString.substring(1));
			System.out.println("Send: " + url.toString());
			HttpURLConnection connection = null;  
			
		    connection = (HttpURLConnection)url.openConnection();
		    connection.setRequestMethod("GET");
		    connection.setRequestProperty("Content-Type", 
		        "application/x-www-form-urlencoded");
		    connection.setUseCaches(false);
		    connection.setDoOutput(true);
		    
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			
			StringBuilder response = new StringBuilder(); 
		    String line;
		    while((line = in.readLine()) != null) {
		      response.append(line);
		    } 
		    
		    System.out.println("Response: " + response.toString());
		    ret = loadOsrmJSON(response.toString());
		    
			in.close();
		
			List<List<Integer>> distance =  ret.getDistanceTable();
			List<List<Double>> source =  ret.getSourceCoordinates();
			List<List<Double>> destination =  ret.getDestinationCoordinates();
			
			matrixBuilder = VehicleRoutingTransportCostsMatrix.Builder.newInstance(true);
			
			for (int ifrom = 0; ifrom < source.size(); ifrom++) {
				for (int ito = 0; ito < destination.size(); ito++) {
					
					if(ifrom != -1){
						/*
						if (pointIds[ito] == ghPoints[ito].toString()){
							ito = ito+1-1;
						} 
						*/
						//matrixBuilder.addTransportDistance(pointIds[ifrom], pointIds[ito], distance.get(ifrom).get(ito) * 0.01 );
		                matrixBuilder.addTransportTime(pointIds[ifrom], pointIds[ito],  distance.get(ifrom).get(ito) * 0.1);
		                //System.out.println(ifrom + ";" + ito + ";" + distance.get(ifrom).get(ito) * 0.05 );
	    				
		                				
					}else{
						//matrixBuilder.addTransportDistance(ghPoints[ifrom].toString(), ghPoints[ito].toString(), Double.POSITIVE_INFINITY);
		                matrixBuilder.addTransportTime(ghPoints[ifrom].toString(), ghPoints[ito].toString(),  Double.POSITIVE_INFINITY);
					}
				}
			}
		
	    } catch (Exception e) {
	    	String status = "ERROR - Error while calling OSRM or parse result: "  +  e.getLocalizedMessage();
	   		routingInformation.addStatus(status);
	       	System.out.println(status);
	       	e.printStackTrace();
			System.out.println( "\n");
			return null;
		}
		
        return matrixBuilder.build();
    }
    

    private static VehicleRoutingTransportCostsMatrix createMatrixOSRMV2(VehicleRoutingProblem.Builder vrpBuilder, RoutingInformation routingInformation) {
    	
    	Set<Entry<String, Coordinate>> list = vrpBuilder.getLocationMap().entrySet();
    	String queryString = new String();
    	OsrmResultV2 ret = null;
    	VehicleRoutingTransportCostsMatrix.Builder matrixBuilder = null;
    	
    	int n = list.size();
 		int i = 0;
    	GHPoint []ghPoints = new GHPoint[n];
        String []pointIds = new String[n];
       
		
    	for(Map.Entry<String, Coordinate> entry:list){
    		queryString += ";" + String.format(Locale.ENGLISH,"%f,%f", entry.getValue().getX(), entry.getValue().getY());
    		ghPoints[i] = new GHPoint(entry.getValue().getY(), entry.getValue().getX());
			pointIds[i] = entry.getKey();
			i++;
		}
    	
		try {
			URL url = new URL("http://" + routingInformation.getParameter().getOsrmIpPort() + "/table/" + routingInformation.getParameter().getOsrmVersion() + "/" + routingInformation.getParameter().getOsrmProfil() + "/" + queryString.substring(1));
			System.out.println("Send: " + url.toString());
			HttpURLConnection connection = null;  
			
		    connection = (HttpURLConnection)url.openConnection();
		    connection.setRequestMethod("GET");
		    connection.setRequestProperty("Content-Type", 
		        "application/x-www-form-urlencoded");
		    connection.setUseCaches(false);
		    connection.setDoOutput(true);
		    
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			
			StringBuilder response = new StringBuilder(); 
		    String line;
		    while((line = in.readLine()) != null) {
		      response.append(line);
		    } 
		    
		    System.out.println("Response: " + response.toString());
		    ret = loadOsrmV2JSON(response.toString());
		    
			in.close();
		
			List<List<Double>> distance = ret.getDurations();
			List<OsrmLocation> sources =  ret.getSources();
			List<OsrmLocation> destinations = ret.getDestinations();
						
			matrixBuilder = VehicleRoutingTransportCostsMatrix.Builder.newInstance(true);
			
			for (int ifrom = 0; ifrom < sources.size(); ifrom++) {
				for (int ito = 0; ito < destinations.size(); ito++) {
					
					if(distance.get(ifrom).get(ito) != null){
						
		                matrixBuilder.addTransportTime(pointIds[ifrom], pointIds[ito],  distance.get(ifrom).get(ito));
		                //System.out.println(ifrom + ";" + ito + ";" + distance.get(ifrom).get(ito) * 0.05 );
	    				
		                				
					}else{
						//matrixBuilder.addTransportDistance(ghPoints[ifrom].toString(), ghPoints[ito].toString(), Double.POSITIVE_INFINITY);
		                matrixBuilder.addTransportTime(ghPoints[ifrom].toString(), ghPoints[ito].toString(),  Double.POSITIVE_INFINITY);
					}
				}
			}
		
	    } catch (Exception e) {
	    	String status = "ERROR - Error while calling OSRM or parse result: "  +  e.getLocalizedMessage();
	   		routingInformation.addStatus(status);
	       	System.out.println(status);
	       	e.printStackTrace();
			System.out.println( "\n");
			return null;
		}
		
        return matrixBuilder.build();
    }
    
    private static Service getService(String serviceId, jsprit.core.problem.VehicleRoutingProblem.Builder vrpBuilder) {
        for (jsprit.core.problem.job.Job j : vrpBuilder.getAddedJobs()) {
            if (j.getId().equals(serviceId)) {
                return (Service) j;
            }
        }
        return null;
    }
    
    private static Shipment getShipment(String serviceId, jsprit.core.problem.VehicleRoutingProblem.Builder vrpBuilder) {
        for (jsprit.core.problem.job.Job j : vrpBuilder.getAddedJobs()) {
            if (j.getId().equals(serviceId)) {
                return (Shipment) j;
            }
        }
        return null;
    }

    private static jsprit.core.problem.vehicle.Vehicle getVehicle(String vehicleId, jsprit.core.problem.VehicleRoutingProblem.Builder vrpBuilder) {
        for (jsprit.core.problem.vehicle.Vehicle v : vrpBuilder.getAddedVehicles()) {
            if (v.getId().equals(vehicleId)) return v;
        }
        return null;
    }


}

