package com.ibring.json;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Stop {

    private Integer stopId;
	private Integer tourStoppIndex;
    private java.util.Date startTime = new Date();
    private java.util.Date endTime = new Date();
    double lon;
    double lat;
    private java.sql.Time  serviceTime = new java.sql.Time  (0);
    private java.sql.Time  waitingTime= new java.sql.Time  (0);
    
    private List<SignedJob> signedJobs = new ArrayList<SignedJob>();
    
    public Stop(int stopId, int index, Double arrTime, Double endTime_, double lon_, double lat_) {
    	this.stopId = stopId;
    	this.tourStoppIndex = index;
    	this.startTime.setTime(arrTime.longValue() * 1000);
    	this.endTime.setTime(endTime_.longValue() * 1000);
    	this.lon = lon_;
    	this.lat = lat_;
	}

	/**
     * 
     * @return
     *     The tourstoppIndex
     */
    public Integer getTourstoppIndex() {
        return tourStoppIndex;
    }

    /**
     * 
     * @param tourstoppIndex
     *     The tourstoppIndex
     */
    public void setTourstoppIndex(Integer tourstoppIndex) {
        this.tourStoppIndex = tourstoppIndex;
    }
    
    public Integer getStopId() {
  		return stopId;
  	}

  	public void setStopId(Integer stopId) {
  		this.stopId = stopId;
  	}

    /**
     * 
     * @return
     *     The startTime
     */
    public java.util.Date getStartTime() {
        return startTime;
    }

    /**
     * 
     * @param startTime
     *     The startTime
     */
    public void setStartTime(java.util.Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 
     * @return
     *     The endTime
     */
    public java.util.Date getEndTime() {
        return endTime;
    }

    /**
     * 
     * @param endTime
     *     The endTime
     */
    public void setEndTime(java.util.Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 
     * @return
     *     The signedJobs
     */
    public List<SignedJob> getSignedJobs() {
        return signedJobs;
    }

    /**
     * 
     * @param signedJobs
     *     The signedJobs
     */
    public void setSignedJobs(List<SignedJob> signedJobs) {
        this.signedJobs = signedJobs;
    }

    public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public java.sql.Time getServiceTime() {
		return serviceTime;
	}
	public void addServiceTime(java.lang.Double serviceTime) {
		if (this.serviceTime.getTime() == 0){
			this.serviceTime.setTime(this.serviceTime.getTimezoneOffset() * 60 * 1000);
		}
		this.serviceTime.setTime(this.serviceTime.getTime() + (serviceTime.longValue() * 1000 ));
	}
	public void setServiceTime(java.sql.Time serviceTime) {
		this.serviceTime = serviceTime;
	}
	public void setServiceTimeSec(java.lang.Double serviceTime) {
		this.serviceTime.setTime(serviceTime.longValue() * 1000 + this.serviceTime.getTimezoneOffset() * 60 * 1000 );   
	}

	public java.sql.Time getWaitingTime() {
		return waitingTime;
	}
	public void addWaitingTime(java.lang.Double waitingTime) {
		if (this.waitingTime.getTime() == 0){
			this.waitingTime.setTime(this.waitingTime.getTimezoneOffset() * 60 * 1000);
		}
		this.waitingTime.setTime(this.waitingTime.getTime() + (waitingTime.longValue() * 1000 ));
	}
	public void setWaitingTime(java.sql.Time waitingTime) {
		this.waitingTime = waitingTime;
	}
	public void setWaitingTimeSec(java.lang.Double waitingTime) {
		this.waitingTime.setTime(waitingTime.longValue() * 1000 + this.waitingTime.getTimezoneOffset() * 60 * 1000 );  
	}
	
	public void addJob(Integer jobId, String type, Double arrTime, Double endTime_, double waitingTime, double serviceTime) {
		
		if(startTime.getTime() > arrTime.longValue() * 1000 ){
			startTime.setTime(arrTime.longValue() * 1000);
		}
		if(endTime.getTime() < endTime_.longValue() * 1000 ){
			endTime.setTime(endTime_.longValue() * 1000);
		}
		this.addWaitingTime(waitingTime);
		this.addServiceTime(serviceTime);
		signedJobs.add(new SignedJob(jobId, type));		
	}


}
