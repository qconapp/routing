package com.ibring.json;

import java.util.ArrayList;
import java.util.List;

public class OsrmResult {
	
	    private List<List<Integer>> distance_table =  new ArrayList<List<Integer>>();
	    private List<List<Double>> source_coordinates =  new ArrayList<List<Double>>();
	    private List<List<Double>> destination_coordinates =  new ArrayList<List<Double>>();
		   
	    public  List<List<Integer>> getDistanceTable() {
	        return distance_table;
	    }
	    
	    public void setDistanceTable(List<List<Integer>> distance_table) {
	        this.distance_table = distance_table;
	    }
	    
	    public void setSourceCoordinates(List<List<Double>> source_coordinates) {
	        this.source_coordinates = source_coordinates;
	    }
	    
	    public  List<List<Double>> getSourceCoordinates() {
	        return source_coordinates;
	    }
	    
	    public  List<List<Double>> getDestinationCoordinates() {
	        return destination_coordinates;
	    }

	    public void setDestinationCoordinates(List<List<Double>> destination_coordinates) {
	        this.destination_coordinates = destination_coordinates;
	    }
		
}