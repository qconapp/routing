package com.ibring.json;

import java.util.ArrayList;
import java.util.List;

public class OsrmLocation {
	
	    private List<Double> location =  new ArrayList<Double>();
	    private String name;
	    private String hint;
		   
	    public  List<Double> getLocation() {
	        return location;
	    }
	    
	    public void setLocation(List<Double> location) {
	        this.location = location;
	    }
	    
	    public String getName() {
			if ( name == null )
				return "";
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		public String getHint() {
			if ( hint == null )
				return "";
			return hint;
		}

		public void setHint(String hint) {
			this.hint = hint;
		}
}