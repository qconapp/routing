package com.ibring.json;

import java.util.ArrayList;
import java.util.List;

public class OsrmResultV2 {
	
	    private List<OsrmLocation> sources =  new ArrayList<OsrmLocation>();
	    private List<OsrmLocation> destinations =  new ArrayList<OsrmLocation>();
	    private String code =  "";
	    private List<List<Double>> durations =  new ArrayList<List<Double>>();
		   
	    public  List<OsrmLocation> getSources() {
	        return sources;
	    }
	    
	    public void setSources(List<OsrmLocation> sources) {
	        this.sources = sources;
	    }
	    
	    public void setDestinations(List<OsrmLocation> destinations) {
	        this.destinations = destinations;
	    }
	    
	    public  List<OsrmLocation> getDestinations() {
	        return destinations;
	    }
	    
	    public String getCode() {
			if ( code == null )
				return "";
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
		
	    public  List<List<Double>> getDurations() {
	        return durations;
	    }

	    public void setDurations(List<List<Double>> durations) {
	        this.durations = durations;
	    }
		
}