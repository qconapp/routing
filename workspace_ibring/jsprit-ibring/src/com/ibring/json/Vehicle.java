package com.ibring.json;

import java.util.List;

public class Vehicle {

    private double fixedCost;
    private List<String> skills;
    private int maxWeight;
    private double lon;
    private int height;
    private int width;
    private int length;
    private double costPerHour;
    private java.util.Date startTime;
    private double costPerKm;
    private double lat;
    private int maxVolume;
    private java.util.Date endTime;
    private String type;
    private long id;
    private int quantity;
    private Location startLocation;
    private Location endLocation;

    /**
     * 
     * @return
     *     The fixedCost
     */
    public double getFixedCost() {
        return fixedCost;
    }

    /**
     * 
     * @param fixedCost
     *     The fixedCost
     */
    public void setFixedCost(long fixedCost) {
        this.fixedCost = fixedCost;
    }

    /**
     * 
     * @return
     *     The skills
     */
    public List<String> getSkills() {
        return skills;
    }

    /**
     * 
     * @param skills
     *     The skills
     */
    public void setSkills( List<String> skills) {
        this.skills = skills;
    }

    /**
     * 
     * @return
     *     The maxWeight
     */
    public int getMaxWeight() {
        return maxWeight;
    }

    /**
     * 
     * @param maxWeight
     *     The maxWeight
     */
    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    /**
     * 
     * @return
     *     The lon
     */
    public double getLon() {
        return lon;
    }

    /**
     * 
     * @param lon
     *     The lon
     */
    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
     * 
     * @return
     *     The height
     */
    public int getHeight() {
        return height;
    }

    /**
     * 
     * @param height
     *     The height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * 
     * @return
     *     The width
     */
    public long getWidth() {
        return width;
    }

    /**
     * 
     * @param width
     *     The width
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * 
     * @return
     *     The length
     */
    public int getLength() {
        return length;
    }

    /**
     * 
     * @param length
     *     The length
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * 
     * @return
     *     The costPerHour
     */
    public double getCostPerHour() {
        return costPerHour;
    }

    /**
     * 
     * @param costPerHour
     *     The costPerHour
     */
    public void setCostPerHour(long costPerHour) {
        this.costPerHour = costPerHour;
    }

    /**
     * 
     * @return
     *     The startTime
     */
    public java.util.Date getStartTime() {
        return startTime;
    }

	public java.lang.Double getStartTimeSec() {
		return startTime.getTime()/1000.0;
	}
	
    /**
     * 
     * @param startTime
     *     The startTime
     */
    public void setStartTime(java.util.Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 
     * @return
     *     The costPerKm
     */
    public double getCostPerKm() {
        return costPerKm;
    }

    /**
     * 
     * @param costPerKm
     *     The costPerKm
     */
    public void setCostPerKm(double costPerKm) {
        this.costPerKm = costPerKm;
    }

    /**
     * 
     * @return
     *     The lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * 
     * @param lat
     *     The lat
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * 
     * @return
     *     The maxVolume
     */
    public int getMaxVolume() {
        return maxVolume;
    }

    /**
     * 
     * @param maxVolume
     *     The maxVolume
     */
    public void setMaxVolume(int maxVolume) {
        this.maxVolume = maxVolume;
    }

    /**
     * 
     * @return
     *     The endTime
     */
    public java.util.Date getEndTime() {
        return endTime;
    }

	public java.lang.Double getEndTimeSec() {
		return endTime.getTime()/1000.0;
	}
	
    /**
     * 
     * @param endTime
     *     The endTime
     */
    public void setEndTime(java.util.Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The id
     */
    public long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * 
     * @param quantity
     *     The quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

	public Location getStartLocation() {
		return startLocation;
	}

	public void setStartLocation(Location startLocation) {
		this.startLocation = startLocation;
	}

	public Location getEndLocation() {
		return endLocation;
	}

	public void setEndLocation(Location endLocation) {
		this.endLocation = endLocation;
	}

}
