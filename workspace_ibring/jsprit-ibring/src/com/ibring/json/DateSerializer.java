package com.ibring.json;

import java.lang.reflect.Type; 
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date; 
import java.util.Locale;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public  class DateSerializer implements JsonDeserializer<Date> ,JsonSerializer<Date>{
		public static final String DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";
	
        @Override
        public Date deserialize(JsonElement jsonElement, Type typeOF,
                                JsonDeserializationContext context) throws JsonParseException {
                try {
                    return new SimpleDateFormat(DATE_FORMAT, Locale.US).parse(jsonElement.getAsString());
                } catch (ParseException e) {
                }

            throw new JsonParseException("Unparseable date: \"" + jsonElement.getAsString()
                    + "\". Supported formats: " + DATE_FORMAT);
        }
        
        @Override
        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(new SimpleDateFormat(DATE_FORMAT, Locale.US).format(src));
        }
}
