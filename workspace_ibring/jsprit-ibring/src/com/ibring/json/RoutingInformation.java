package com.ibring.json;

import java.util.ArrayList;
import java.util.List;

public class RoutingInformation {
	
	    private List<Vehicle> vehicles = new ArrayList<Vehicle>();
	    private List<Job> jobs = new ArrayList<Job>();
	    private List<Tour> tours = new ArrayList<Tour>();
	    private List<Integer> unassignedJobs = new ArrayList<Integer>();
	    private Parameter parameter;
	    private String status = "";
	    
	    /**
	     * 
	     * @return
	     *     The vehicles
	     */
	    public List<Vehicle> getVehicles() {
	        return vehicles;
	    }

	    /**
	     * 
	     * @param vehicles
	     *     The vehicles
	     */
	    public void setVehicles(List<Vehicle> vehicles) {
	        this.vehicles = vehicles;
	    }

	    /**
	     * 
	     * @return
	     *     The jobs
	     */
	    public List<Job> getJobs() {
	        return jobs;
	    }

	    /**
	     * 
	     * @param jobs
	     *     The jobs
	     */
	    public void setJobs(List<Job> jobs) {
	        this.jobs = jobs;
	    }

	    /**
	     * 
	     * @return
	     *     The tours
	     */
	    public List<Tour> getTours() {
	        return tours;
	    }

	    /**
	     * 
	     * @param tours
	     *     The tours
	     */
	    public void setTours(List<Tour> tours) {
	        this.tours = tours;
	    }

	    /**
	     * 
	     * @return
	     *     The unassignedJobs
	     */
	    public List<Integer> getUnassignedJobs() {
	        return unassignedJobs;
	    }

	    /**
	     * 
	     * @param unassignedJobs
	     *     The unassignedJobs
	     */
	    public void setUnassignedJobs(List<Integer> unassignedJobs) {
	        this.unassignedJobs = unassignedJobs;
	    }

	    /**
	     * 
	     * @return
	     *     The parameter
	     */
	    public Parameter getParameter() {
	        return parameter;
	    }

	    /**
	     * 
	     * @param parameter
	     *     The parameter
	     */
	    public void setParameter(Parameter parameter) {
	        this.parameter = parameter;
	    }

		public void addTour(Tour tour) {
			tours.add(tour);
			
		}

		public void addUnassignedJobs(Integer jobId) {
			unassignedJobs.add(jobId);
			
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
		
		public void addStatus(String status) {
			this.status += '\n' + status;
		}
	    
		
}