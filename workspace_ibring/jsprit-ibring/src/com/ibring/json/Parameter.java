package com.ibring.json;

public class Parameter {

    private boolean depotBasedDelivery;
    private double multipleLoadingFactor;
    private double multipleUnloadingFactor;
    private double multipleTransferringFactor;
    private String graphhopperMaps;
    private String osrmIpPort;
    private String osrmVersion;
    private String osrmProfile;
    private boolean fleetSizeFinite;
    private int jobLimit;
    private int packageLimit;
    private double kmLimit;
    private double hourLimit;
    private int iterations;
    private int threads;
    
	/**
     * 
     * @return
     *     The depotBasedDelivery
     */
    public boolean isDepotBasedDelivery() {
        return depotBasedDelivery;
    }

    /**
     * 
     * @param depotBasedDelivery
     *     The depotBasedDelivery
     */
    public void setDepotBasedDelivery(boolean depotBasedDelivery) {
        this.depotBasedDelivery = depotBasedDelivery;
    }

    /**
     * 
     * @return
     *     The multipleLoadingFactor
     */
    public double getMultipleLoadingFactor() {
    	if (multipleLoadingFactor == 0.0){
    		return 1.0;
    	}
        return multipleLoadingFactor;
    }

    /**
     * 
     * @param multipleLoadingFactor
     *     The multipleLoadingFactor
     */
    public void setMultipleLoadingFactor(double multipleLoadingFactor) {
        this.multipleLoadingFactor = multipleLoadingFactor;
    }

    /**
     * 
     * @return
     *     The multipleUnloadingFactor
     */
    public double getMultipleUnloadingFactor() {
    	if (multipleUnloadingFactor == 0.0){
    		return 1.0;
    	}
        return multipleUnloadingFactor;
    }

    /**
     * 
     * @param multipleUnloadingFactor
     *     The multipleUnloadingFactor
     */
    public void setMultipleUnloadingFactor(double multipleUnloadingFactor) {
        this.multipleUnloadingFactor = multipleUnloadingFactor;
    }

    /**
     * 
     * @return
     *     The multipleTransferringFactor
     */
    public double getMultipleTransferringFactor() {
    	if (multipleTransferringFactor == 0.0){
    		return 1.0;
    	}
        return multipleTransferringFactor;
    }

    /**
     * 
     * @param multipleTransferringFactor
     *     The multipleTransferringFactor
     */
    public void setMultipleTransferringFactor(double multipleTransferringFactor) {
        this.multipleTransferringFactor = multipleTransferringFactor;
    }

	public String getGraphhopperMaps() {
		if ( graphhopperMaps == null )
			return "";
		return graphhopperMaps;
	}

	public void setGraphhopperMaps(String graphhopperMaps) {
		this.graphhopperMaps = graphhopperMaps;
	}
	
	public String getOsrmIpPort() {
		if ( osrmIpPort == null )
			return "";
		return osrmIpPort;
	}
	public void setOsrmIpPort(String osrmIpPort) {
		this.osrmIpPort = osrmIpPort;
	}

	public void setOsrmVersion(String osrmVersion) {
		this.osrmVersion = osrmVersion;
	}
	public String getOsrmVersion() {
		if ( osrmVersion == null )
			return "v1";
		return osrmVersion;
	}
	
	public String getOsrmProfil() {
		if ( osrmProfile == null )
			return "car";
		return osrmProfile;
	}
	public void setOsrmProfil(String osrmProfile) {
		this.osrmProfile = osrmProfile;
	}
	
    public boolean isFleetSizeFinite() {
		return fleetSizeFinite;
	}

	public void setFleetSizeFinite(boolean fleetSizeFinite) {
		this.fleetSizeFinite = fleetSizeFinite;
	}

	public int getJobLimit() {
		return jobLimit;
	}

	public void setJobLimit(int jobLimit) {
		this.jobLimit = jobLimit;
	}

	public int getPackageLimit() {
		return packageLimit;
	}

	public void setPackageLimit(int packageLimit) {
		this.packageLimit = packageLimit;
	}

	public double getKmLimit() {
		return kmLimit;
	}

	public void setKmLimit(double kmLimit) {
		this.kmLimit = kmLimit;
	}

	public int getIterations() {
		return iterations;
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	public int getThreads() {
		return threads;
	}

	public void setThreads(int threads) {
		this.threads = threads;
	}
	public double getHourLimit() {
		return hourLimit;
	}

	public void setHourLimit(double hourLimit) {
		this.hourLimit = hourLimit;
	}

}
