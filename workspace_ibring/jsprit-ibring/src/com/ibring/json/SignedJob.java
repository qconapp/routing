package com.ibring.json;

public class SignedJob {

    private String type;
    private long jobId;

    public SignedJob(Integer jobId2, String type2) {
    	jobId = jobId2;
    	type = type2;
	}

	/**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The jobId
     */
    public long getJobId() {
        return jobId;
    }

    /**
     * 
     * @param jobId
     *     The jobId
     */
    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

}
