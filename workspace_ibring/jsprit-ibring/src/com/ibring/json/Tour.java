package com.ibring.json;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import jsprit.core.problem.Location;

public class Tour {

    private long tourId;
    private long vehicleId;
    private java.util.Date startTime = new Date();
    private java.util.Date endTime  = new Date();
    private java.sql.Time  operationTime= new java.sql.Time  (0);
    private java.sql.Time  serviceTime = new java.sql.Time  (0);
    private java.sql.Time  waitingTime= new java.sql.Time  (0);
    private java.sql.Time  transportTime= new java.sql.Time  (0);
    private java.sql.Time timeWindowViolation= new java.sql.Time  (0);
    private double totalDistance;
    private double transportCosts;
    private double fixedCosts;
    private double variableTransportCosts;
     
    private List<Stop> stops = new ArrayList<Stop>();
     
    //private  LinkedHashMap<Location, Stop> stopsValue =  new LinkedHashMap<Location, Stop>();

    public Tour(long tourId, long vehicleId) {
		super();
		this.tourId = tourId;
		this.vehicleId = vehicleId;
	}
    
    /**
     * 
     * @return
     *     The tourId
     */
    public long getTourId() {
        return tourId;
    }

    /**
     * 
     * @param tourId
     *     The tourId
     */
    public void setTourId(long tourId) {
        this.tourId = tourId;
    }

    /**
     * 
     * @return
     *     The vehicleId
     */
    public long getVehicleId() {
        return vehicleId;
    }

    /**
     * 
     * @param vehicleId
     *     The vehicleId
     */
    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public java.util.Date getStartTime() {
		return startTime;
	}

	public void setStartTime(java.util.Date startTime) {
		this.startTime = startTime;
	}
	
	public void  setStartTimeSec(java.lang.Double startTimeSec) {
		startTime.setTime(startTimeSec.longValue() * 1000);
	}
	 

	public java.util.Date getEndTime() {
		return endTime;
	}

	public void setEndTime(java.util.Date endTime) {
		this.endTime = endTime;
	}

	public void  setEndTimeSec(java.lang.Double endTimeSec) {
		endTime.setTime(endTimeSec.longValue() * 1000);
	}
	
	public java.sql.Time getOperationTime() {
		return operationTime;
	}

	public void setOperationTime(java.sql.Time operationTime) {
		this.operationTime = operationTime;
	}
	public void setOperationTimeSec(java.lang.Double operationTime) {
		this.operationTime.setTime(operationTime.longValue() * 1000 + this.operationTime.getTimezoneOffset() * 60 * 1000 );  
	}
	
	public java.sql.Time getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(java.sql.Time serviceTime) {
		this.serviceTime = serviceTime;
	}
	public void setServiceTimeSec(java.lang.Double serviceTime) {
		this.serviceTime.setTime(serviceTime.longValue() * 1000 + this.serviceTime.getTimezoneOffset() * 60 * 1000 );   
	}

	public java.sql.Time getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(java.sql.Time waitingTime) {
		this.waitingTime = waitingTime;
	}
	public void setWaitingTimeSec(java.lang.Double waitingTime) {
		this.waitingTime.setTime(waitingTime.longValue() * 1000 + this.waitingTime.getTimezoneOffset() * 60 * 1000 );  
	}

	public java.sql.Time getTransportTime() {
		return transportTime;
	}

	public void setTransportTime(java.sql.Time transportTime) {
		this.transportTime = transportTime;
	}
	public void setTransportTimeSec(java.lang.Double transportTime) {
		this.transportTime.setTime(transportTime.longValue() * 1000 + this.transportTime.getTimezoneOffset() * 60 * 1000 );  
	}

	public java.sql.Time getTimeWindowViolation() {
		return timeWindowViolation;
	}

	public void setTimeWindowViolation(java.sql.Time timeWindowViolation) {
		this.timeWindowViolation = timeWindowViolation;
	}
	public void setTimeWindowViolationSec(java.lang.Double timeWindowViolation) { 
		this.timeWindowViolation.setTime(timeWindowViolation.longValue() * 1000 + this.timeWindowViolation.getTimezoneOffset() * 60 * 1000 );  
	}

	public double getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}

	public double getTransportCosts() {
		return transportCosts;
	}

	public void setTransportCosts(double transportCosts) {
		this.transportCosts = transportCosts;
	}

	public double getFixedCosts() {
		return fixedCosts;
	}

	public void setFixedCosts(double fixedCosts) {
		this.fixedCosts = fixedCosts;
	}

	public double getVariableTransportCosts() {
		return variableTransportCosts;
	}

	public void setVariableTransportCosts(double variableTransportCosts) {
		this.variableTransportCosts = variableTransportCosts;
	}

	/**
     * 
     * @return
     *     The stops
     */
    public List<Stop> getStops() {
        return stops;
    }

    /**
     * 
     * @param stops
     *     The stops
     */
    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

    public void addJob(SignedJob job){
    	//stopsValue.put( job, value)
    	
    }

	public void addJob(int stopId, int jobId, String type,int index, double arrTime, double endTime, double lon, double lat, double waitingTime, double serviceTime) {
		Stop stop = null;
		if (stops.size() > 0){
			if (stops.get(stops.size() - 1).getLon() == lon && stops.get(stops.size() - 1).getLat() == lat) {
				stop = stops.get(stops.size() - 1);
			}
		}

		if ( stop == null){
			stop = new Stop(stopId, stops.size() + 1,  arrTime, endTime, lon, lat );
			stops.add(stop);
			
		}
		System.out.println("<trkpt lon=\"" + lon + "\" lat=\"" + lat + "\"/>");
		stop.addJob(jobId, type, arrTime, endTime, waitingTime, serviceTime);
	}

}
