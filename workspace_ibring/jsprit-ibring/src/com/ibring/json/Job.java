package com.ibring.json;

public class Job {

    private int weight;
    private java.sql.Time  transferring;
    private double height;
    private Delivery delivery;
    private long id;
    private int volume;
    private java.sql.Time  loading;
    private double length;
    private double width;
    private Pickup pickup;
    private String type;
    private java.sql.Time  unloading;
    private int quantity;

    /**
     * 
     * @return
     *     The weight
     */
    public int getWeight() {
        return weight;
    }

    /**
     * 
     * @param weight
     *     The weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * 
     * @return
     *     The transferring
     */
    public java.sql.Time  getTransferring() {
        return transferring;
    }

	public java.lang.Double getTransferringSec() { 
		
		return (transferring.toLocalTime().toSecondOfDay())*1.0;
	}
	
    /**
     * 
     * @param transferring
     *     The transferring
     */
    public void setTransferring(java.sql.Time  transferring) {
        this.transferring = transferring;
    }

    /**
     * 
     * @return
     *     The height
     */
    public double getHeight() {
        return height;
    }

    /**
     * 
     * @param height
     *     The height
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * 
     * @return
     *     The delivery
     */
    public Delivery getDelivery() {
        return delivery;
    }

    /**
     * 
     * @param delivery
     *     The delivery
     */
    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    /**
     * 
     * @return
     *     The id
     */
    public long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The volume
     */
    public int getVolume() {
        return volume;
    }

    /**
     * 
     * @param volume
     *     The volume
     */
    public void setVolume(int volume) {
        this.volume = volume;
    }

    /**
     * 
     * @return
     *     The loading
     */
    public java.sql.Time  getLoading() {
        return loading;
    }

	public java.lang.Double getLoadingSec() { 
		
		return (loading.toLocalTime().toSecondOfDay())*1.0;
	}
	
    /**
     * 
     * @param loading
     *     The loading
     */
    public void setLoading(java.sql.Time  loading) {
        this.loading = loading;
    }

    /**
     * 
     * @return
     *     The length
     */
    public double getLength() {
        return length;
    }

    /**
     * 
     * @param length
     *     The length
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * 
     * @return
     *     The width
     */
    public double getWidth() {
        return width;
    }

    /**
     * 
     * @param width
     *     The width
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * 
     * @return
     *     The pickup
     */
    public Pickup getPickup() {
        return pickup;
    }

    /**
     * 
     * @param pickup
     *     The pickup
     */
    public void setPickup(Pickup pickup) {
        this.pickup = pickup;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The unloading
     */
    public java.sql.Time  getUnloading() {
        return unloading;
    }

	public java.lang.Double getUnloadingSec() { 
		
		return (unloading.toLocalTime().toSecondOfDay())*1.0;
	}
	
    /**
     * 
     * @param unloading
     *     The unloading
     */
    public void setUnloading(java.sql.Time  unloading) {
        this.unloading = unloading;
    }

    /**
     * 
     * @return
     *     The quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * 
     * @param quantity
     *     The quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


}
