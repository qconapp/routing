package com.ibring.json;

import java.util.Date;

public class Pickup {

    private String street;
    private java.util.Date startTime;
    private double lat;
    private String houseNumber;
    private String city;
    private String name;
    private String zip;
    private String country;
    private double lon;
    private java.util.Date endTime;
    private String type;

    /**
     * 
     * @return
     *     The street
     */
    public String getStreet() {
        return street;
    }

    /**
     * 
     * @param street
     *     The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * 
     * @return
     *     The startTime
     */
    public java.util.Date getStartTime() {
        return startTime;
    }

	public java.lang.Double getStartTimeSec() {
		Date tmp = startTime;
		//return (tmp.getTime() - new Date(tmp.getYear(),tmp.getMonth(),tmp.getDay(),0,0,0).getTime())/1000.0;
		return (tmp.getTime())/1000.0;
	}
	
    /**
     * 
     * @param startTime
     *     The startTime
     */
    public void setStartTime(java.util.Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 
     * @return
     *     The lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * 
     * @param lat
     *     The lat
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * 
     * @return
     *     The houseNumber
     */
    public String getHouseNumber() {
        return houseNumber;
    }

    /**
     * 
     * @param houseNumber
     *     The houseNumber
     */
    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    /**
     * 
     * @return
     *     The city
     */
    public String getCity() {
        return city;
    }

    /**
     * 
     * @param city
     *     The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * 
     * @param zip
     *     The zip
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * 
     * @return
     *     The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The lon
     */
    public double getLon() {
        return lon;
    }

    /**
     * 
     * @param lon
     *     The lon
     */
    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
     * 
     * @return
     *     The endTime
     */
    public java.util.Date getEndTime() {
        return endTime;
    }

	public java.lang.Double getEndTimeSec() {
		Date tmp = endTime;
		//return (tmp.getTime() - new Date(tmp.getYear(),tmp.getMonth(),tmp.getDay(),0,0,0).getTime())/1000.0;
		return (tmp.getTime())/1000.0;
	}
	
    /**
     * 
     * @param endTime
     *     The endTime
     */
    public void setEndTime(java.util.Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

}
