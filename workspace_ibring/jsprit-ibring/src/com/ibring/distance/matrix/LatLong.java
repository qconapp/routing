package com.ibring.distance.matrix;

import com.graphhopper.util.shapes.GHPoint;

public class LatLong extends GHPoint{

	public double getLatitude(){
		return this.lat;
	}
	
	public double getLongitude(){
		return this.lon;
	}
	 
}