package com.ibring.Constraints;

import jsprit.core.algorithm.state.StateUpdater;
import jsprit.core.problem.solution.route.VehicleRoute;
import jsprit.core.problem.solution.route.activity.TourActivity;
import jsprit.core.problem.solution.route.RouteVisitor;

public class ServiceTimeUpdater implements StateUpdater, RouteVisitor {

    @Override
    public void visit(VehicleRoute route) {
        if (route.getVehicle() != null) {
           setServiceTimes(route);
        }
    }

    private void setServiceTimes(VehicleRoute route) {
    	TourActivity prevAct = null;
    	
    	 for (TourActivity act : route.getTourActivities().getActivities()) {
    		 if (prevAct != null) {
	    		 if (act.getClass().equals( prevAct.getClass()) &&
	    			 act.getLocation().equals(prevAct.getLocation()) ) {
	    			act.setRepeatedOperationTime(true);
	    		 }else{
	    			act.setRepeatedOperationTime(false);
	    		 }
    		 }else{
    			 act.setRepeatedOperationTime(false); 
    		 }
    		 prevAct = act;
         }
    }
}
