package com.ibring.Constraints;

import jsprit.core.algorithm.state.StateId;
import jsprit.core.algorithm.state.StateManager;
import jsprit.core.problem.constraint.HardActivityConstraint;
import jsprit.core.problem.misc.JobInsertionContext;
import jsprit.core.problem.solution.route.activity.TourActivity;
import jsprit.core.util.VehicleRoutingTransportCostsMatrix;

public class DistanceConstraint implements HardActivityConstraint {

    private final StateManager stateManager;

    private final VehicleRoutingTransportCostsMatrix costsMatrix;

    private final double maxDistance;

    //        private final StateFactory.StateId distanceStateId; //v1.3.1
    private final StateId distanceStateId; //head of development - upcoming release (v1.4)

    //        DistanceConstraint(double maxDistance, StateFactory.StateId distanceStateId, StateManager stateManager, VehicleRoutingTransportCostsMatrix costsMatrix) { //v1.3.1
    public DistanceConstraint(double maxDistance, StateId distanceStateId, StateManager stateManager, VehicleRoutingTransportCostsMatrix transportCosts) { //head of development - upcoming release (v1.4)
        this.costsMatrix = transportCosts;
        this.maxDistance = maxDistance;
        this.stateManager = stateManager;
        this.distanceStateId = distanceStateId;
    }

    @Override
    public ConstraintsStatus fulfilled(JobInsertionContext context, TourActivity prevAct, TourActivity newAct, TourActivity nextAct, double v) {
        double additionalDistance = getDistance(prevAct, newAct) + getDistance(newAct, nextAct) - getDistance(prevAct, nextAct);
        Double routeDistance = stateManager.getRouteState(context.getRoute(), distanceStateId, Double.class);
        if (routeDistance == null) routeDistance = 0.;
        double newRouteDistance = routeDistance + additionalDistance;
        if (newRouteDistance > maxDistance) {
            return ConstraintsStatus.NOT_FULFILLED;
        } else return ConstraintsStatus.FULFILLED;
    }

    double getDistance(TourActivity from, TourActivity to) {
        return costsMatrix.getDistance(from.getLocation().getId(), to.getLocation().getId());
    }

}
