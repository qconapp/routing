package com.ibring.Constraints;

import jsprit.core.algorithm.state.StateId;
import jsprit.core.algorithm.state.StateManager;
import jsprit.core.algorithm.state.StateUpdater;
import jsprit.core.problem.solution.route.VehicleRoute;
import jsprit.core.problem.solution.route.activity.ActivityVisitor;
import jsprit.core.problem.solution.route.activity.TourActivity;
import jsprit.core.util.VehicleRoutingTransportCostsMatrix;

public class TransportTimeUpdater implements StateUpdater, ActivityVisitor {

    private final StateManager stateManager;
    private final  VehicleRoutingTransportCostsMatrix transportCosts;
    private final StateId transportTimeStateId;
    private VehicleRoute vehicleRoute;

    private double transportTime = 0.;

    private TourActivity prevAct;

    public TransportTimeUpdater(StateId transportTimeStateId, StateManager stateManager, VehicleRoutingTransportCostsMatrix transportCosts) { //head of development - upcoming release (v1.4)
        this.transportCosts = transportCosts;
        this.stateManager = stateManager;
        this.transportTimeStateId = transportTimeStateId;
    }

    @Override
    public void begin(VehicleRoute vehicleRoute) {
    	transportTime = 0.;
        prevAct = vehicleRoute.getStart();
        this.vehicleRoute = vehicleRoute;
    }

    @Override
    public void visit(TourActivity tourActivity) {
    	transportTime += transportCosts.getTransportTime(prevAct.getLocation(), tourActivity.getLocation(), 0., null, null);
    	prevAct = tourActivity;
    }

    @Override
    public void finish() {
    	transportTime += transportCosts.getTransportTime(prevAct.getLocation(), vehicleRoute.getEnd().getLocation(), 0., null, null);
        stateManager.putRouteState(vehicleRoute, transportTimeStateId, transportTime); //head of development - upcoming release (v1.4)
    }

}

