package com.ibring.Constraints;

import jsprit.core.algorithm.state.StateId;
import jsprit.core.algorithm.state.StateManager;
import jsprit.core.problem.constraint.HardActivityConstraint;
import jsprit.core.problem.misc.JobInsertionContext;
import jsprit.core.problem.solution.route.activity.TourActivity;
import jsprit.core.util.VehicleRoutingTransportCostsMatrix;

public class TransportTimeConstraint implements HardActivityConstraint {

    private final StateManager stateManager;
    private final VehicleRoutingTransportCostsMatrix transportCosts;
    private final double maxTransportTime;

    private final StateId transportTimeStateId; //head of development - upcoming release (v1.4)

    public TransportTimeConstraint(double maxTransportTime, StateId transportTimeStateId, StateManager stateManager, VehicleRoutingTransportCostsMatrix transportCosts) { //head of development - upcoming release (v1.4)
        this.transportCosts = transportCosts;
        this.maxTransportTime = maxTransportTime;
        this.stateManager = stateManager;
        this.transportTimeStateId = transportTimeStateId;
    }

    @Override
    public ConstraintsStatus fulfilled(JobInsertionContext context, TourActivity prevAct, TourActivity newAct, TourActivity nextAct, double v) {
    	
        double additionalTransportTime = transportCosts.getTransportTime(prevAct.getLocation(), newAct.getLocation(), 0., null, null) 
        				+ transportCosts.getTransportTime(newAct.getLocation(), nextAct.getLocation(), 0., null, null)
        				- transportCosts.getTransportTime(prevAct.getLocation(), nextAct.getLocation(), 0., null, null);
        Double routeTransportTime = stateManager.getRouteState(context.getRoute(), transportTimeStateId, Double.class);
        if (routeTransportTime == null) routeTransportTime = 0.;
        double newRouteTransportTime = routeTransportTime + additionalTransportTime;
        if (newRouteTransportTime > maxTransportTime) {
            return ConstraintsStatus.NOT_FULFILLED;
        } else return ConstraintsStatus.FULFILLED;
    }


}
